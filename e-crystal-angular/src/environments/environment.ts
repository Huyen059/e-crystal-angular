// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // serverBaseUrl: 'http://localhost:9500/e-crystal/api/v1/',
  // erpTimesheets: {
  //   name: 'erpTimesheets',
  //   timesheet: {
  //     name: 'Name',
  //     user: 'User',
  //     month: 'Month',
  //     year: 'Year',
  //     status: 'Status',
  //     totalHours: 'Total hours',
  //     totalManDays: 'Total man days',
  //     employee: 'Employee',
  //     externalId: 'External ID',
  //     findByNameEq: 'Find by name',
  //     findAllTimesheets: 'All',
  //     findByUserEq: 'Find by user',
  //     findByMonthEq_YearEq: 'Find by year and month',
  //     findByUserEq_YearEq_MonthEq: 'Find by user and year and month',
  //     findByStatusEq: 'Find by status',
  //     findByEmployeeEq: 'Find by employee',
  //     findByExternalIdEq: 'Find by external id',
  //   }
  // },
  // enterprise: {
  //   name: 'enterprise',
  // }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
