import { Component, OnInit } from '@angular/core';
import {TimesheetEntryService} from '../_services/timesheet-entry.service';


@Component({
  selector: 'app-timesheet-entry-workspace',
  templateUrl: './timesheet-entry-workspace.component.html',
  styleUrls: ['./timesheet-entry-workspace.component.css']
})
export class TimesheetEntryWorkspaceComponent implements OnInit {

  constructor(
    private timesheetEntryService: TimesheetEntryService
  ) { }

  ngOnInit(): void {
    this.timesheetEntryService.resetFetchItemConditions();
    this.timesheetEntryService.fetchItemConditions.sortBy = null;
    this.timesheetEntryService.pageUrl = null;
  }

}
