import {Component, OnInit} from '@angular/core';
import {TimesheetEntryOutputModel} from '../_models/TimesheetEntryOutputModel';
import {LinksModel} from '../../../_models/LinksModel';
import {PageModel} from '../../../_models/PageModel';
import {ActivatedRoute, Router} from '@angular/router';
import {TimesheetEntryService} from '../_services/timesheet-entry.service';
import {TimesheetOutputModel} from '../../timesheet/_models/TimesheetOutputModel';
import {TimesheetEntry} from '../_models/TimesheetEntry';
import {ProjectOutputModel} from '../../../enterprise/project/_models/ProjectOutputModel';
import {SubprojectOutputModel} from '../../../enterprise/subproject/_models/SubprojectOutputModel';
import {NavigationService} from '../../../_services/navigation.service';
import {AuthService} from '../../../_services/auth.service';

@Component({
  selector: 'app-timesheet-entry-as-sublist',
  templateUrl: './timesheet-entry-as-sublist.component.html',
  styleUrls: ['./timesheet-entry-as-sublist.component.css']
})
export class TimesheetEntryAsSublistComponent implements OnInit {

  timesheetEntries: TimesheetEntryOutputModel[] = [];
  private links: LinksModel;
  page: PageModel;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private timesheetEntryService: TimesheetEntryService,
    private navigationService: NavigationService,
    private authService: AuthService,
  ) { }

  get currentUser() {
    return this.authService.currentUserValue;
  }

  get fields(): any {
    return this.timesheetEntryService.fields;
  }

  get fetchItemConditions(): TimesheetEntry {
    return this.timesheetEntryService.fetchItemConditions;
  }

  ngOnInit(): void {
    this.getTimesheetEntriesOnInit();
  }

  private getTimesheetEntriesOnInit() {
    this.setFilterOnInitValues();
    this.getTimesheetEntries();
  }

  /* ----- HTTP REQUESTS -----*/

  private getTimesheetEntries(): void {
    this.timesheetEntryService.getTimesheetEntries()
      .subscribe(
        data => {
          this.extractData(data);
        }
      );
  }

  private getTimesheetEntriesByPageUrl(url: string): void {
    this.timesheetEntryService.getTimesheetEntriesByPageUrl(url)
      .subscribe(
        data => {
          this.extractData(data);
        }
      );
  }

  delete(externalId: string): void {
    this.timesheetEntryService.delete(externalId)
      .subscribe(() => this.getTimesheetEntries());
  }

  /* ----- NAVIGATION -----*/

  goToTimesheetEntryDetailPage(externalId: string): void {
    this.router.navigate(['timesheetentries', externalId]);
  }

  goToAddTimesheetEntryForm(): void {
    let data = {};

    if (this.navigationService.parentElementFullName === 'erpTimesheets.timesheet') {
      data = {
        timesheet: this.navigationService.parentExternalId
      };
    }

    if (this.navigationService.parentElementFullName === 'enterprise.project') {
      data = {
        project: this.navigationService.parentExternalId
      };
    }

    if (this.navigationService.parentElementFullName === 'enterprise.subproject') {
      data = {
        subproject: this.navigationService.parentExternalId
      };
    }

    this.router.navigate(['new', data], {relativeTo: this.route});
  }

  goToEditTimesheetEntryForm(externalId: string): void {
    this.router.navigate(['edit', {externalId}], {relativeTo: this.route});
  }

  /* ----- PAGINATION ----- */

  displayFirstPageOfList(): void {
    this.getTimesheetEntriesByPageUrl(this.links.first.href);
  }

  displayPreviousPageOfList(): void {
    if (this.links.prev) {
      this.getTimesheetEntriesByPageUrl(this.links.prev.href);
    }
  }

  displayNextPageOfList(): void {
    if (this.links.next) {
      this.getTimesheetEntriesByPageUrl(this.links.next.href);
    }
  }

  displayLastPageOfList(): void {
    this.getTimesheetEntriesByPageUrl(this.links.last.href);
  }

  /* ----- FILTER ----- */

  /*  filter on init  */

  private setFilterOnInitValues(): void {
    if (this.navigationService.parentElementFullName === 'erpTimesheets.timesheet') {
      this.timesheetEntryService.fetchItemConditions.timesheet = new TimesheetOutputModel();
      this.timesheetEntryService.fetchItemConditions.timesheet.externalId = this.navigationService.parentExternalId;
    }

    if (this.navigationService.parentElementFullName === 'enterprise.project') {
      this.timesheetEntryService.fetchItemConditions.project = new ProjectOutputModel();
      this.timesheetEntryService.fetchItemConditions.project.externalId = this.navigationService.parentExternalId;
    }

    if (this.navigationService.parentElementFullName === 'enterprise.subproject') {
      this.timesheetEntryService.fetchItemConditions.subproject = new SubprojectOutputModel();
      this.timesheetEntryService.fetchItemConditions.subproject.externalId = this.navigationService.parentExternalId;
    }
  }

  /* ----- SORT ----- */

  getSortCssClass(sortField: string): string {
    if (sortField !== this.timesheetEntryService.fetchItemConditions.sortBy) {
      return 'fas fa-sort';
    }

    if (this.timesheetEntryService.fetchItemConditions.isAscending) {
      return 'fas fa-sort-down';
    }

    return 'fas fa-sort-up';
  }

  enableSort(sortField: string): void {
    if (this.timesheetEntryService.fetchItemConditions.sortBy === sortField) {
      this.switchSortDirection();
    } else {
      this.setSortField(sortField);
    }

    this.sort();
  }

  private setSortField(sortField: string): void {
    this.timesheetEntryService.fetchItemConditions.sortBy = sortField;
    this.timesheetEntryService.fetchItemConditions.isAscending = true;
  }

  private switchSortDirection(): void {
    this.timesheetEntryService.fetchItemConditions.isAscending = !this.timesheetEntryService.fetchItemConditions.isAscending;
  }

  private sort(): void {
    this.getTimesheetEntries();
  }

  clearSort() {
    this.timesheetEntryService.fetchItemConditions.sortBy = null;
    this.getTimesheetEntries();
  }

  /* ----- MISC ----- */

  private extractData(data): void {
    this.timesheetEntries = data._embedded.timesheetEntries;
    this.links = data._links;
    this.page = data._page;
  }
}
