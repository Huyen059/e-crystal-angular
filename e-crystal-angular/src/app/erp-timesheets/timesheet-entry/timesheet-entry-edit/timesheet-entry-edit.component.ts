import {AfterViewInit, Component, OnInit} from '@angular/core';
import {TimesheetEntry} from '../_models/TimesheetEntry';
import {TimesheetOutputModel} from '../../timesheet/_models/TimesheetOutputModel';
import {ProjectOutputModel} from '../../../enterprise/project/_models/ProjectOutputModel';
import {SubprojectOutputModel} from '../../../enterprise/subproject/_models/SubprojectOutputModel';
import {TimesheetEntryService} from '../_services/timesheet-entry.service';
import {TimesheetService} from '../../timesheet/_services/timesheet.service';
import {ProjectService} from '../../../enterprise/project/_services/project.service';
import {SubprojectService} from '../../../enterprise/subproject/_services/subproject.service';
import {FilterService} from '../../../filter/_services/filter.service';
import {Location} from '@angular/common';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {TimesheetEntryMapper} from '../_models/TimesheetEntryMapper';
import {TimesheetEntryPutInputMapper} from '../_models/TimesheetEntryPutInputMapper';
import {Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, switchMap} from 'rxjs/operators';
import {AuthService} from '../../../_services/auth.service';

@Component({
  selector: 'app-timesheet-entry-edit',
  templateUrl: './timesheet-entry-edit.component.html',
  styleUrls: ['./timesheet-entry-edit.component.css']
})
export class TimesheetEntryEditComponent implements OnInit, AfterViewInit {
  timesheetEntry: TimesheetEntry = new TimesheetEntry();

  // for each linked field
  timesheets: TimesheetOutputModel[] = [];
  isTimesheetOptionsShown = false;

  // for each linked field
  projects: ProjectOutputModel[] = [];
  isProjectOptionsShown = false;

  // for each linked field
  subprojects: SubprojectOutputModel[] = [];
  isSubprojectOptionsShown = false;

  // custom
  originalTimesheet: TimesheetOutputModel;
  originalProject: ProjectOutputModel;

  isFilterMenuShown = false;

  // for dynamic search
  private searchTerm = new Subject<string>();

  constructor(
    private timesheetEntryService: TimesheetEntryService,
    private timesheetService: TimesheetService,
    private projectService: ProjectService,
    private subprojectService: SubprojectService,
    private filterService: FilterService,
    private route: ActivatedRoute,
    private location: Location,
    private authService: AuthService,
  ) {  }

  get currentUser() {
    return this.authService.currentUserValue;
  }

  // for each linked field
  get chosenTimesheet(): TimesheetOutputModel {
    if (this.filterService.hasSelectedItem('erpTimesheets.timesheet')) {
      this.timesheetEntry.timesheet = this.filterService.currentElement().selectedItem.value;
      this.filterService.currentElement().selectedItem = null;
    }
    // custom: set date field: start
    if (this.timesheetEntry.timesheet && this.timesheetEntry.timesheet !== this.originalTimesheet) {
      this.setTimesheetEntryDisplayDate(this.timesheetEntry.timesheet.month, this.timesheetEntry.timesheet.year);
    }
    // custom: set date field: end
    return this.timesheetEntry.timesheet;
  }

  // for each linked field
  get chosenProject(): ProjectOutputModel {
    if (this.filterService.hasSelectedItem('enterprise.project')) {
      this.timesheetEntry.project = this.filterService.currentElement().selectedItem.value;
      this.filterService.currentElement().selectedItem = null;
    }
    // custom: restrict subprojects: start
    if (this.timesheetEntry.project && this.timesheetEntry.project !== this.originalProject) {
      this.subprojectService.fetchItemConditions.project = this.timesheetEntry.project.externalId;
    }
    // custom: restrict subprojects: end
    return this.timesheetEntry.project;
  }

  // for each linked field
  get chosenSubproject(): SubprojectOutputModel {
    if (this.filterService.hasSelectedItem('enterprise.subproject')) {
      this.timesheetEntry.subproject = this.filterService.currentElement().selectedItem.value;
      this.filterService.currentElement().selectedItem = null;
    }
    return this.timesheetEntry.subproject;
  }

  ngOnInit(): void {
    this.filterService.resetFilterData();
    this.getTimesheetEntry();
  }

  ngAfterViewInit(): void {
    this.cancelDialog();
  }

  private cancelDialog() {
    window.onclick = this.closeDropdownsOnClickOutside();
    window.onkeyup = this.closeDropdownsOnPressingEscape();
  }

  private closeDropdownsOnClickOutside() {
    const timesheetDropdown = document.querySelector('.timesheet-dropdown') as HTMLElement;
    const projectDropdown = document.querySelector('.project-dropdown') as HTMLElement;
    const subprojectDropdown = document.querySelector('.subproject-dropdown') as HTMLElement;
    return (e) => {
      if (!timesheetDropdown.contains(e.target)) {
        this.isTimesheetOptionsShown = false;
      }

      if (!projectDropdown.contains(e.target)) {
        this.isProjectOptionsShown = false;
      }

      if (!subprojectDropdown.contains(e.target)) {
        this.isSubprojectOptionsShown = false;
      }
    };
  }

  private closeDropdownsOnPressingEscape() {
    return e => {
      if (e.key === 'Escape') {
        this.isTimesheetOptionsShown = false;
        this.isProjectOptionsShown = false;
        this.isSubprojectOptionsShown = false;
      }
    };
  }

  /* ----- HTTP REQUEST ----- */

  private getTimesheetEntry() {
    this.route.paramMap
      .subscribe(
        (params: ParamMap) => {
          const externalId = params.get('externalId');
          this.timesheetEntryService.getTimesheetEntryByExternalId(externalId)
            .subscribe(
              timesheetEntry => {
                this.timesheetEntry = TimesheetEntryMapper.map(timesheetEntry);
                // custom
                this.originalTimesheet = this.timesheetEntry.timesheet;
                this.originalProject = this.timesheetEntry.project;
              }
            );
        }
      );
  }

  saveTimesheetEntry(): void {
    this.timesheetEntryService.saveTimesheetEntry(TimesheetEntryPutInputMapper.map(this.timesheetEntry))
      .subscribe(
        () => this.location.back()
      );
  }

  // for each linked field
  private getTimesheets(): void {
    this.timesheetService.getTimesheets()
      .subscribe(
        data => this.timesheets = data._embedded.timesheets
      );
  }

  // for each linked field
  private getProjects(): void {
    this.projectService.getProjects()
      .subscribe(
        data => this.projects = data._embedded.projects
      );
  }

  // for each linked field
  private getSubprojects(): void {
    this.subprojectService.getSubprojects()
      .subscribe(
        data => this.subprojects = data._embedded.subprojects
      );
  }

  /* ----- DROPDOWN INPUT ----- */

  toggleDropdownOptions(elementName: string): void {
    if (elementName === 'erpTimesheets.timesheet') {
      if (!this.isTimesheetOptionsShown) {
        this.getTimesheets();
        this.isTimesheetOptionsShown = true;
      } else {
        this.isTimesheetOptionsShown = false;
      }
    }

    if (elementName === 'enterprise.project') {
      if (!this.isProjectOptionsShown) {
        this.getProjects();
        this.isProjectOptionsShown = true;
      } else {
        this.isProjectOptionsShown = false;
      }
    }

    if (elementName === 'enterprise.subproject') {
      if (!this.isSubprojectOptionsShown) {
        this.getSubprojects();
        this.isSubprojectOptionsShown = true;
      } else {
        this.isSubprojectOptionsShown = false;
      }
    }
  }

  private openDropdownOptions(elementName: string): void {
    if (elementName === 'erpTimesheets.timesheet') {
      this.isTimesheetOptionsShown = true;
    }

    if (elementName === 'enterprise.project') {
      this.isProjectOptionsShown = true;
    }

    if (elementName === 'enterprise.subproject') {
      this.isSubprojectOptionsShown = true;
    }
  }

 private closeDropdownOptions(elementName: string): void {
    if (elementName === 'erpTimesheets.timesheet') {
      this.isTimesheetOptionsShown = false;
    }

    if (elementName === 'enterprise.project') {
      this.isProjectOptionsShown = false;
    }

    if (elementName === 'enterprise.subproject') {
      this.isSubprojectOptionsShown = false;
    }
  }

  chooseADropdownOption(
    elementName: string,
    option:
      TimesheetOutputModel |
      ProjectOutputModel |
      SubprojectOutputModel
  ): void {
    if (elementName === 'erpTimesheets.timesheet') {
      this.isTimesheetOptionsShown = false;
      this.timesheetEntry.timesheet = option as TimesheetOutputModel;
    }

    if (elementName === 'enterprise.project') {
      this.isProjectOptionsShown = false;
      this.timesheetEntry.project = option as ProjectOutputModel;
    }

    if (elementName === 'enterprise.subproject') {
      this.isSubprojectOptionsShown = false;
      this.timesheetEntry.subproject = option as SubprojectOutputModel;
    }
  }

  showFilterComponentForCurrentField(elementName: string): void {
    this.closeDropdownOptions(elementName);
    this.isFilterMenuShown = true;
    this.filterService.resetFilterData();
    this.filterService.addFilter(elementName);
  }

  clearInputField(elementName: string) {
    if (elementName === 'erpTimesheets.timesheet') {
      this.timesheetEntry.timesheet = null;
    }

    if (elementName === 'enterprise.project') {
      this.timesheetEntry.project = null;
    }

    if (elementName === 'enterprise.subproject') {
      this.timesheetEntry.subproject = null;
    }
  }

  searchHandler(elementName: string, $event: KeyboardEvent) {
    this.openDropdownOptions(elementName);
    const input = ($event.target as HTMLInputElement).value;
    this.searchTerm.next(input);

    this.searchTerm.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      switchMap((term: string) => {
        if (elementName === 'erpTimesheets.timesheet') {
          return this.timesheetService.search(term);
        }

        if (elementName === 'enterprise.project') {
          return this.projectService.search(term);
        }

        if (elementName === 'enterprise.subproject') {
          return this.subprojectService.search(term);
        }

      })
    ).subscribe(data => {
      if (elementName === 'erpTimesheets.timesheet') {
        this.timesheets = data._embedded.timesheets;
      }

      if (elementName === 'enterprise.project') {
        this.projects = data._embedded.projects;
      }

      if (elementName === 'enterprise.subproject') {
        this.subprojects = data._embedded.subprojects;
      }

    });
  }

  /* ----- CUSTOM : EVENT LISTENERS AND THEIR SUPPORTED FUNCTIONS ----- */

  // when select a certain timesheet, display the corresponding month, year in the input for date
  private setTimesheetEntryDisplayDate(month: number, year: number): void {
    const today = new Date();
    const timesheetDate = new Date();
    timesheetDate.setFullYear(year, month - 1, 1);
    if (timesheetDate > today) {
      this.timesheetEntry.date = this.convertDateToValueAttribute(timesheetDate);
    } else {
      this.timesheetEntry.date = this.convertDateToValueAttribute(today);
    }
  }

  private convertDateToValueAttribute(defaultDate: Date): string {
    return defaultDate.toISOString().substr(0, 10);
  }

}
