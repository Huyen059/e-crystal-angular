import {AfterViewInit, Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {TimesheetEntryService} from '../_services/timesheet-entry.service';
import {TimesheetService} from '../../timesheet/_services/timesheet.service';
import {ProjectService} from '../../../enterprise/project/_services/project.service';
import {SubprojectService} from '../../../enterprise/subproject/_services/subproject.service';
import {ProjectOutputModel} from '../../../enterprise/project/_models/ProjectOutputModel';
import {SubprojectOutputModel} from '../../../enterprise/subproject/_models/SubprojectOutputModel';
import {Location} from '@angular/common';
import {TimesheetOutputModel} from '../../timesheet/_models/TimesheetOutputModel';
import {TimesheetEntry} from '../_models/TimesheetEntry';
import {FilterService} from '../../../filter/_services/filter.service';
import {TimesheetEntryPostInputMapper} from '../_models/TimesheetEntryPostInputMapper';
import {debounceTime, distinctUntilChanged, switchMap} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {AuthService} from '../../../_services/auth.service';

@Component({
  selector: 'app-timesheet-entry-new',
  templateUrl: './timesheet-entry-new.component.html',
  styleUrls: ['./timesheet-entry-new.component.css']
})
export class TimesheetEntryNewComponent implements OnInit, AfterViewInit {
  timesheetEntry: TimesheetEntry = new TimesheetEntry();

  // for each linked field
  timesheets: TimesheetOutputModel[] = [];
  isTimesheetOptionsShown = false;

  // for each linked field
  projects: ProjectOutputModel[] = [];
  isProjectOptionsShown = false;

  // for each linked field
  subprojects: SubprojectOutputModel[] = [];
  isSubprojectOptionsShown = false;

  isFilterMenuShown = false;

  // for dynamic search
  private searchTerm = new Subject<string>();

  // custom
  // boolean values used in "check box" to allow user to create multiple entries at the same time
  isMultipleDays = false;
  isHourDifferentByDay = false;

  // custom
  // for form display
  numberOfDays: number;
  dates: Date[] = [];

  constructor(
    private timesheetEntryService: TimesheetEntryService,
    private timesheetService: TimesheetService,
    private projectService: ProjectService,
    private subprojectService: SubprojectService,
    private filterService: FilterService,
    private location: Location,
    private authService: AuthService,
    // custom
    private route: ActivatedRoute,
  ) {  }

  get currentUser() {
    return this.authService.currentUserValue;
  }

  // for each linked field
  get chosenTimesheet(): TimesheetOutputModel {
    if (this.filterService.hasSelectedItem('erpTimesheets.timesheet')) {
      this.timesheetEntry.timesheet = this.filterService.currentElement().selectedItem.value;
      this.filterService.currentElement().selectedItem = null;
    }
    // custom: set date field: start
    if (this.timesheetEntry.timesheet) {
      this.setTimesheetEntryDisplayDate(this.timesheetEntry.timesheet.month, this.timesheetEntry.timesheet.year);
    }
    // custom: set date field: end
    return this.timesheetEntry.timesheet;
  }

  // for each linked field
  get chosenProject(): ProjectOutputModel {
    if (this.filterService.hasSelectedItem('enterprise.project')) {
      this.timesheetEntry.project = this.filterService.currentElement().selectedItem.value;
      this.filterService.currentElement().selectedItem = null;
    }
    // custom: restrict subprojects: start
    if (this.timesheetEntry.project) {
      this.subprojectService.fetchItemConditions.project = this.timesheetEntry.project.externalId;
    }
    // custom: restrict subprojects: end
    return this.timesheetEntry.project;
  }

  // for each linked field
  get chosenSubproject(): SubprojectOutputModel {
    if (this.filterService.hasSelectedItem('enterprise.subproject')) {
      this.timesheetEntry.subproject = this.filterService.currentElement().selectedItem.value;
      this.filterService.currentElement().selectedItem = null;
    }
    return this.timesheetEntry.subproject;
  }

  ngOnInit(): void {
    this.filterService.resetFilterData();
    // custom
    this.prefillForm();
  }

  ngAfterViewInit(): void {
    this.cancelDialog();
  }

  private cancelDialog() {
    window.onclick = this.closeDropdownsOnClickOutside();
    window.onkeyup = this.closeDropdownsOnPressingEscape();
  }

  private closeDropdownsOnClickOutside() {
    const timesheetDropdown = document.querySelector('.timesheet-dropdown') as HTMLElement;
    const projectDropdown = document.querySelector('.project-dropdown') as HTMLElement;
    const subprojectDropdown = document.querySelector('.subproject-dropdown') as HTMLElement;
    return (e) => {
      if (!timesheetDropdown.contains(e.target)) {
        this.isTimesheetOptionsShown = false;
      }

      if (!projectDropdown.contains(e.target)) {
        this.isProjectOptionsShown = false;
      }

      if (!subprojectDropdown.contains(e.target)) {
        this.isSubprojectOptionsShown = false;
      }
    };
  }

  private closeDropdownsOnPressingEscape() {
    return e => {
      if (e.key === 'Escape') {
        this.isTimesheetOptionsShown = false;
        this.isProjectOptionsShown = false;
        this.isSubprojectOptionsShown = false;
      }
    };
  }

  // custom
  private prefillForm(): void {
    this.fillTimesheet();
    this.fillDate();
  }

  // custom
  // for each linked field of which the current element is a data child
  private fillTimesheet(): void {
    // If we create entry for a certain timesheet, we will fill the input for timesheet
    // and set a default date to the correct month and year
    const timesheetExternalId = this.route.snapshot.paramMap.get('timesheet');
    if (timesheetExternalId !== null) {
      this.timesheetService.getTimesheetByExternalId(timesheetExternalId)
        .subscribe(
          timesheet => {
            this.timesheetEntry.timesheet = timesheet;
            // Add this chosen timesheet to the dropdown option, it will be the only one there
            this.timesheets.push(timesheet);
            // set default date to specific date depending on the date of timesheet
            this.setTimesheetEntryDisplayDate(timesheet.month, timesheet.year);
          }
        );
    }
  }

  // custom
  private fillDate(): void {
    // If there's no chosen timesheet yet, display the list of timesheet for user to choose
    if (this.timesheetEntry.timesheet === null) {
      // set default date to today
      this.timesheetEntry.date = this.convertDateToValueAttribute(new Date());
    }
  }

  /* ----- HTTP REQUEST ----- */

  addTimesheetEntry(): void {
    // custom
    if (this.isMultipleDays && this.timesheetEntry.hoursByDay == null) {
      this.timesheetEntry.hoursByDay = [];
      for (let i = 0; i < this.numberOfDays; i++) {
        this.timesheetEntry.hoursByDay[i] = this.timesheetEntry.hours;
      }
    }

    this.timesheetEntryService.createTimesheetEntry(TimesheetEntryPostInputMapper.map(this.timesheetEntry))
      .subscribe(() => this.location.back());
  }

  // for each linked field
  private getTimesheets(): void {
    this.timesheetService.getTimesheets()
      .subscribe(
        data => this.timesheets = data._embedded.timesheets
      );
  }

  // for each linked field
  private getProjects(): void {
    this.projectService.getProjects()
      .subscribe(
        data => this.projects = data._embedded.projects
      );
  }

  // for each linked field
  private getSubprojects(): void {
    // custom
    if (!this.timesheetEntry.project) {
      return;
    }

    this.subprojectService.getSubprojects()
      .subscribe(
        data => this.subprojects = data._embedded.subprojects
      );
  }

  /* ----- DROPDOWN INPUT ----- */

  toggleDropdownOptions(elementName: string): void {
    if (elementName === 'erpTimesheets.timesheet') {
      if (!this.isTimesheetOptionsShown) {
        this.getTimesheets();
        this.isTimesheetOptionsShown = true;
      } else {
        this.isTimesheetOptionsShown = false;
      }
    }

    if (elementName === 'enterprise.project') {
      if (!this.isProjectOptionsShown) {
        this.getProjects();
        this.isProjectOptionsShown = true;
      } else {
        this.isProjectOptionsShown = false;
      }
    }

    if (elementName === 'enterprise.subproject') {
      if (!this.isSubprojectOptionsShown) {
        this.getSubprojects();
        this.isSubprojectOptionsShown = true;
      } else {
        this.isSubprojectOptionsShown = false;
      }
    }
  }

  private openDropdownOptions(elementName: string): void {
    if (elementName === 'erpTimesheets.timesheet') {
      this.isTimesheetOptionsShown = true;
    }

    if (elementName === 'enterprise.project') {
      this.isProjectOptionsShown = true;
    }

    if (elementName === 'enterprise.subproject') {
      this.isSubprojectOptionsShown = true;
    }
  }

  private closeDropdownOptions(elementName: string): void {
    if (elementName === 'erpTimesheets.timesheet') {
      this.isTimesheetOptionsShown = false;
    }

    if (elementName === 'enterprise.project') {
      this.isProjectOptionsShown = false;
    }

    if (elementName === 'enterprise.subproject') {
      this.isSubprojectOptionsShown = false;
    }
  }

  chooseADropdownOption(
    elementName: string,
    option:
      TimesheetOutputModel |
      ProjectOutputModel |
      SubprojectOutputModel
  ): void {
    if (elementName === 'erpTimesheets.timesheet') {
      this.isTimesheetOptionsShown = false;
      this.timesheetEntry.timesheet = option as TimesheetOutputModel;
    }

    if (elementName === 'enterprise.project') {
      this.isProjectOptionsShown = false;
      this.timesheetEntry.project = option as ProjectOutputModel;
    }

    if (elementName === 'enterprise.subproject') {
      this.isSubprojectOptionsShown = false;
      this.timesheetEntry.subproject = option as SubprojectOutputModel;
    }
  }

  showFilterComponentForCurrentField(elementName: string): void {
    this.closeDropdownOptions(elementName);
    this.isFilterMenuShown = true;
    this.filterService.resetFilterData();
    this.filterService.addFilter(elementName);
  }

  clearInputField(elementName: string) {
    if (elementName === 'erpTimesheets.timesheet') {
      this.timesheetEntry.timesheet = null;
      // custom
      this.fillDate();
    }

    if (elementName === 'enterprise.project') {
      this.timesheetEntry.project = null;
      // custom
      this.subprojects = [];
    }

    if (elementName === 'enterprise.subproject') {
      this.timesheetEntry.subproject = null;
    }
  }

  searchHandler(elementName: string, $event: KeyboardEvent) {
    this.openDropdownOptions(elementName);
    const input = ($event.target as HTMLInputElement).value;
    this.searchTerm.next(input);

    this.searchTerm.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      switchMap((term: string) => {
        if (elementName === 'erpTimesheets.timesheet') {
          return this.timesheetService.search(term);
        }

        if (elementName === 'enterprise.project') {
          return this.projectService.search(term);
        }

        if (elementName === 'enterprise.subproject') {
          return this.subprojectService.search(term);
        }

      })
    ).subscribe(data => {
      if (elementName === 'erpTimesheets.timesheet') {
        this.timesheets = data._embedded.timesheets;
      }

      if (elementName === 'enterprise.project') {
        this.projects = data._embedded.projects;
      }

      if (elementName === 'enterprise.subproject') {
        this.subprojects = data._embedded.subprojects;
      }

    });
  }

  /* ----- CUSTOM : EVENT LISTENERS AND THEIR SUPPORTED FUNCTIONS ----- */

  // when select a certain timesheet, display the corresponding month, year in the input for date
  private setTimesheetEntryDisplayDate(month: number, year: number): void {
    const today = new Date();
    const timesheetDate = new Date();
    timesheetDate.setFullYear(year, month - 1, 1);
    if (timesheetDate > today) {
      this.timesheetEntry.date = this.convertDateToValueAttribute(timesheetDate);
    } else {
      this.timesheetEntry.date = this.convertDateToValueAttribute(today);
    }
  }

  // Display forms for multiple days/hours

  displayChooseMultipleDaysForm(): void {
    this.isMultipleDays = !this.isMultipleDays;
    this.switchDateValues();
  }

  displayEditHourByDayForm(): void {
    this.isHourDifferentByDay = !this.isHourDifferentByDay;
    this.switchHoursValues();
  }

  private switchDateValues(): void {
    const dateInput = document.querySelector('#date') as HTMLInputElement;
    if (this.isMultipleDays) {
      this.timesheetEntry.date = dateInput.value;
      this.timesheetEntry.startDate = this.timesheetEntry.date;
      this.timesheetEntry.endDate = this.timesheetEntry.date;
    }

    if (!this.isMultipleDays) {
      this.timesheetEntry.date = this.timesheetEntry.startDate;
      dateInput.value = this.timesheetEntry.date;
      this.timesheetEntry.startDate = null;
      this.timesheetEntry.endDate = null;
      this.timesheetEntry.hoursByDay = null;
    }
  }

  switchHoursValues(): void {
    if (this.isHourDifferentByDay) {
      this.timesheetEntry.hoursByDay = [];
      for (let i = 0; i < this.numberOfDays; i++) {
        this.timesheetEntry.hoursByDay.push(this.timesheetEntry.hours);
      }
      this.timesheetEntry.hours = 0;
    }

    if (!this.isHourDifferentByDay) {
      if (this.timesheetEntry.hoursByDay != null) {
        this.timesheetEntry.hours = this.timesheetEntry.hoursByDay[0];
      }
      this.timesheetEntry.hoursByDay = null;
    }
  }

  // React when user change start and/or end dates

  updateDatesArray(): void {
    const startDate = new Date(this.timesheetEntry.startDate);
    const endDate = new Date(this.timesheetEntry.endDate);
    this.numberOfDays = this.calculateNumberOfDays(startDate, endDate);
    this.dates = [];
    for (let i = 0; i < this.numberOfDays; i++) {
      const nextDate = new Date();
      nextDate.setFullYear(startDate.getFullYear(), startDate.getMonth());
      nextDate.setDate(startDate.getDate() + i);
      this.dates.push(nextDate);
    }
  }

  private calculateNumberOfDays(startDate: Date, endDate: Date): number {
    return this.countDaysBetween(startDate, endDate);
  }

  private countDaysBetween(startDate: Date, endDate: Date): number {
    // The number of milliseconds in all UTC days (no DST)
    const oneDay = 1000 * 60 * 60 * 24;

    // A day in UTC always lasts 24 hours (unlike in other time formats)
    const start = Date.UTC(endDate.getFullYear(), endDate.getMonth(), endDate.getDate());
    const end = Date.UTC(startDate.getFullYear(), startDate.getMonth(), startDate.getDate());

    // so it's safe to divide by 24 hours
    return Math.round((start - end) / oneDay) + 1;
  }

  private convertDateToValueAttribute(defaultDate: Date): string {
    return defaultDate.toISOString().substr(0, 10);
  }

  trackByIdx(index: number, obj: any): any {
    return index;
  }
}
