import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import {TimesheetEntryOutputModel} from '../_models/TimesheetEntryOutputModel';
import {TimesheetEntry} from '../_models/TimesheetEntry';
import {TimesheetEntryPostInputModel} from '../_models/TimesheetEntryPostInputModel';
import {PaginatedResponse} from '../_models/PaginatedResponse';
import {TimesheetEntryPutInputModel} from '../_models/TimesheetEntryPutInputModel';
import {AlertService} from '../../../alert/_services/alert.service';
import {AuthService} from '../../../_services/auth.service';


@Injectable({
  providedIn: 'root'
})
export class TimesheetEntryService {

  private apiBaseUrl = 'http://localhost:9500/e-crystal/api/v1/' + 'erpTimesheets' + '/timesheetentries';
  private httpReadOptions = {headers: { Authorization : `Basic ${this.authorizationData}` }};
  private httpWriteOptions = {headers: { Authorization : `Basic ${this.authorizationData}`, 'Content-Type': 'application/json' }};

  fields = {
    project: 'project',
    subproject: 'subproject',
    timesheet: 'timesheet',
    date: 'date',
    hours: 'hours',
    status: 'status',
    description: 'description',
    expense: 'expense',
    workingMonth: 'workingMonth',
    employee: 'employee',
  };

  filterOptions = [
    this.fields.employee,
    this.fields.project,
    this.fields.subproject,
    this.fields.timesheet,
    this.fields.status,
    this.fields.workingMonth,
  ];

  defaultFilterOptions: string[] = [
    this.fields.project,
    this.fields.subproject,
    this.fields.employee,
  ];

  chosenFilterOptions: string[] = [
    this.fields.project,
    this.fields.subproject,
    this.fields.employee,
  ];

  fetchItemConditions: TimesheetEntry = new TimesheetEntry();
  private _hasFilter: boolean;
  pageUrl: string;

  constructor(
    private http: HttpClient,
    private alertService: AlertService,
    private authService: AuthService,
  ) { }

  get authorizationData(): string {
    return this.authService.currentUserValue ?
      this.authService.currentUserValue.authorizationData : '';
  }

  get hasFilter(): boolean {
    return this._hasFilter;
  }

  /* ----- READ ----- */

  getTimesheetEntries(): Observable<PaginatedResponse> {
    if (this.authService.currentUserValue) {
      let url = this.apiBaseUrl;
      this.updateHasFilter();
      if (this.hasFilter || this.fetchItemConditions.sortBy) {
        url += '?' + TimesheetEntryService.createQueryParameters(this.fetchItemConditions);
      }
      return this.http.get<PaginatedResponse>(url, this.httpReadOptions)
        .pipe(
          catchError(this.handleError('Get timesheet entries', new PaginatedResponse()))
        );
    }
    return of(new PaginatedResponse());
  }

  getTimesheetEntryByExternalId(selectedId: string): Observable<TimesheetEntryOutputModel> {
    if (this.authService.currentUserValue) {
      const url = this.apiBaseUrl + `/${selectedId}`;
      return this.http.get<TimesheetEntryOutputModel>(url, this.httpReadOptions)
        .pipe(
          catchError(this.handleError('Get timesheet entry by external id', new TimesheetEntryOutputModel()))
        );
    }
    return of(new TimesheetEntryOutputModel());
  }

  getTimesheetEntriesByPageUrl(url: string): Observable<PaginatedResponse> {
    if (this.authService.currentUserValue) {
      return this.http.get<PaginatedResponse>(url, this.httpReadOptions)
        .pipe(
          catchError(this.handleError('Get timesheet entries by page url', new PaginatedResponse()))
        );
    }
    return of(new PaginatedResponse());
  }

  search(term: string): Observable<PaginatedResponse> {
    if (this.authService.currentUserValue) {
      this.fetchItemConditions.name = term.trim();
      return this.getTimesheetEntries();
    }
    return of(new PaginatedResponse());
  }

  /* ----- CREATE ----- */

  createTimesheetEntry(timesheetEntry: TimesheetEntryPostInputModel): Observable<any> {
    if (this.authService.currentUserValue) {
      const url = this.apiBaseUrl;
      return this.http.post(url, timesheetEntry, this.httpWriteOptions)
        .pipe(
          tap(_ => this.logSuccess('Timesheet entry created')),
          catchError(this.handleError('Get timesheet entry by external id'))
        );
    }
    this.handleError('Login to perform this operation', of(null));
  }

  /* ----- UPDATE ----- */

  saveTimesheetEntry(timesheetEntry: TimesheetEntryPutInputModel): Observable<any> {
    if (this.authService.currentUserValue) {
      const url = this.apiBaseUrl + `/${timesheetEntry.externalId}`;
      return this.http.put(url, timesheetEntry, this.httpWriteOptions)
        .pipe(
          tap(_ => this.logSuccess('Timesheet entry updated')),
          catchError(this.handleError('Update timesheet entry'))
        );
    }
    this.handleError('Login to perform this operation', of(null));
  }

  /* ----- DELETE ----- */

  delete(externalId: string): Observable<any> {
    if (this.authService.currentUserValue) {
    const url = this.apiBaseUrl + `/${externalId}`;
    return this.http.delete(url, this.httpWriteOptions)
      .pipe(
        tap(_ => this.logSuccess('Timesheet entry deleted')),
        catchError(this.handleError('Delete timesheet entry'))
      );
    }
    this.handleError('Login to perform this operation', of(null));
  }

  /* ----- ERROR HANDLING ----- */

  private handleError<T>(operation = 'Operation', result?: T): (error: any) => Observable<T> {
    return (error: any): Observable<T> => {
      this.logError(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

  /* ----- LOGGING ----- */

  private logSuccess(message: string): void {
    this.alertService.success(`TimesheetService: ${message}`, { autoClose: true });
  }

  private logError(message: string): void {
    this.alertService.error(`TimesheetService: ${message}`, { autoClose: true });
  }

  /* ----- FILTER ----- */

  private static createQueryParameters(fetchItemConditions: TimesheetEntry): string {
    const parameters: string[] = [];

    if (fetchItemConditions.status) {
      parameters.push('status=' + fetchItemConditions.status);
    }

    if (fetchItemConditions.timesheet) {
      parameters.push('timesheet=' + fetchItemConditions.timesheet.externalId);
    }

    if (fetchItemConditions.project) {
      parameters.push('project=' + fetchItemConditions.project.externalId);
    }

    if (fetchItemConditions.subproject) {
      parameters.push('subproject=' + fetchItemConditions.subproject.externalId);
    }

    if (fetchItemConditions.workingMonth) {
      parameters.push('workingMonth=' + fetchItemConditions.workingMonth.externalId);
    }

    if (fetchItemConditions.employee) {
      parameters.push('employee=' + fetchItemConditions.employee.externalId);
    }

    if (fetchItemConditions.sortBy) {
      parameters.push('sortBy=' + fetchItemConditions.sortBy);
    }

    if (fetchItemConditions.isAscending !== null) {
      if (fetchItemConditions.isAscending) {
        parameters.push('isAscending=true');
      } else {
        parameters.push('isAscending=false');
      }
    }

    return parameters.join('&');
  }

  findIndexOfChosenFilterOption(fieldName: string): number {
    return this.chosenFilterOptions.findIndex(chosenOption => chosenOption === fieldName);
  }

  resetDefaultFilterOptions(): void {
    this.chosenFilterOptions = this.defaultFilterOptions;
  }

  resetFilter(): void {
    this.filterOptions.forEach(option => {
      this.fetchItemConditions[option] = null;
    });
    this.resetDefaultFilterOptions();
    this._hasFilter = false;
  }

  resetFetchItemConditions(): void {
    this.fetchItemConditions = new TimesheetEntry();
  }

  updateHasFilter(): void {
    this._hasFilter = false;
    for (const option of this.filterOptions) {
      if (this.fetchItemConditions[option] !== null) {
        this._hasFilter = true;
        break;
      }
    }
  }
}
