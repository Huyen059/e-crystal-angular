import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TimesheetEntryOutputModel} from '../_models/TimesheetEntryOutputModel';
import {TimesheetEntryService} from '../_services/timesheet-entry.service';
import {LinksModel} from '../../../_models/LinksModel';
import {PageModel} from '../../../_models/PageModel';
import {TimesheetEntry} from '../_models/TimesheetEntry';
import {FilterService} from '../../../filter/_services/filter.service';
import {PaginatedResponse} from '../_models/PaginatedResponse';
import {AuthService} from '../../../_services/auth.service';

@Component({
  selector: 'app-timesheet-entry-list',
  templateUrl: './timesheet-entry-list.component.html',
  styleUrls: ['./timesheet-entry-list.component.css']
})
export class TimesheetEntryListComponent implements OnInit {
  private elementFullName = 'erpTimesheets.timesheetEntry';

  timesheetEntries: TimesheetEntryOutputModel[] = [];
  private links: LinksModel;
  page: PageModel;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private timesheetEntryService: TimesheetEntryService,
    private filterService: FilterService,
    private authService: AuthService,
  ) { }

  get currentUser() {
    return this.authService.currentUserValue;
  }

  get fields(): any {
    return this.timesheetEntryService.fields;
  }

  get fetchItemConditions(): TimesheetEntry {
    return this.timesheetEntryService.fetchItemConditions;
  }

  ngOnInit(): void {
    this.filterService.resetFilterData();
    this.filterService.addFilter(this.elementFullName);
    this.getTimesheetEntriesOnInit();
  }

  private getTimesheetEntriesOnInit(): void {
    if (this.timesheetEntryService.pageUrl) {
      this.getTimesheetEntriesByPageUrl(this.timesheetEntryService.pageUrl);
    } else {
      this.getTimesheetEntries();
    }
  }

  /* ----- HTTP REQUESTS -----*/

  private getTimesheetEntries(): void {
    this.timesheetEntryService.getTimesheetEntries()
      .subscribe(
        (data: PaginatedResponse) => {
          this.extractData(data);
        }
      );
  }

  private getTimesheetEntriesByPageUrl(url: string): void {
    this.timesheetEntryService.getTimesheetEntriesByPageUrl(url)
      .subscribe(
        (data: PaginatedResponse) => {
          this.extractData(data);
        }
      );
  }

  delete(externalId: string): void {
    this.timesheetEntryService.delete(externalId)
      .subscribe(() => this.getTimesheetEntriesOnInit());
  }

  /* ----- NAVIGATION -----*/

  goToTimesheetEntryDetailPage(externalId: string): void {
    this.router.navigate(['timesheetentries/' + externalId + '/expenses']);
  }

  goToAddTimesheetEntryForm(): void {
    this.router.navigate(['timesheetentries/new']);
  }

  goToEditTimesheetEntryForm(externalId: string): void {
    this.router.navigate(['timesheetentries/edit', {externalId}]);
  }

  /* ----- PAGINATION ----- */

  displayFirstPageOfList(): void {
    this.getTimesheetEntriesByPageUrl(this.links.first.href);
  }

  displayPreviousPageOfList(): void {
    if (this.links.prev) {
      this.getTimesheetEntriesByPageUrl(this.links.prev.href);
    }
  }

  displayNextPageOfList(): void {
    if (this.links.next) {
      this.getTimesheetEntriesByPageUrl(this.links.next.href);
    }
  }

  displayLastPageOfList(): void {
    this.getTimesheetEntriesByPageUrl(this.links.last.href);
  }

  /* ----- FILTER ----- */

  /*  filter event catchers  */

  onFilter(dataFromApi: any): void {
    this.extractData(dataFromApi);
  }

  /* ----- SORT ----- */

  getSortCssClass(sortField: string): string {
    if (sortField !== this.timesheetEntryService.fetchItemConditions.sortBy) {
      return 'fas fa-sort';
    }

    if (this.timesheetEntryService.fetchItemConditions.isAscending) {
      return 'fas fa-sort-down';
    }

    return 'fas fa-sort-up';
  }

  enableSort(sortField: string): void {
    if (this.timesheetEntryService.fetchItemConditions.sortBy === sortField) {
      this.switchSortDirection();
    } else {
      this.setSortField(sortField);
    }

    this.sort();
  }

  private setSortField(sortField: string): void {
    this.timesheetEntryService.fetchItemConditions.sortBy = sortField;
    this.timesheetEntryService.fetchItemConditions.isAscending = true;
  }

  private switchSortDirection(): void {
    this.timesheetEntryService.fetchItemConditions.isAscending = !this.timesheetEntryService.fetchItemConditions.isAscending;
  }

  private sort(): void {
    this.getTimesheetEntries();
  }

  clearSort() {
    this.timesheetEntryService.fetchItemConditions.sortBy = null;
    this.getTimesheetEntries();
  }

  /* ----- MISC ----- */

  private extractData(data): void {
    this.timesheetEntries = data._embedded.timesheetEntries;
    this.links = data._links;
    this.page = data._page;
    if (this.links) {
      this.timesheetEntryService.pageUrl = this.links.self.href;
    }
  }
}
