import {TimesheetEntry} from './TimesheetEntry';
import {TimesheetEntryPutInputModel} from './TimesheetEntryPutInputModel';

export class TimesheetEntryPutInputMapper {
  static map(timesheetEntry: TimesheetEntry): TimesheetEntryPutInputModel {
    const timesheetEntryPutInputModel = new TimesheetEntryPutInputModel();
    timesheetEntryPutInputModel.date = timesheetEntry.date ? timesheetEntry.date : null;
    timesheetEntryPutInputModel.hours = timesheetEntry.hours ? timesheetEntry.hours : null;
    timesheetEntryPutInputModel.description = timesheetEntry.description ? timesheetEntry.description : null;
    timesheetEntryPutInputModel.status = timesheetEntry.status ? timesheetEntry.status : null;
    timesheetEntryPutInputModel.externalId = timesheetEntry.externalId ? timesheetEntry.externalId : null;
    timesheetEntryPutInputModel.timesheet = timesheetEntry.timesheet ? timesheetEntry.timesheet.externalId : null;
    timesheetEntryPutInputModel.project = timesheetEntry.project ? timesheetEntry.project.externalId : null;
    timesheetEntryPutInputModel.subproject = timesheetEntry.subproject ? timesheetEntry.subproject.externalId : null;

    return timesheetEntryPutInputModel;
  }
}
