export class TimesheetEntryPostInputModel {
  constructor(
    public date: string = null,
    public hours: number = null,
    public description: string = null,
    public status: string = null,
    public externalId: string = null,
    public timesheet: string = null,
    public project: string = null,
    public subproject: string = null,
    // custom
    public startDate: string = null,
    public endDate: string = null,
    public hoursByDay: number[] = null,
  ) {  }
}
