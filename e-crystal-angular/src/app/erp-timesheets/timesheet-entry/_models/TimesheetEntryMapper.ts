import {TimesheetEntryOutputModel} from './TimesheetEntryOutputModel';
import {TimesheetEntry} from './TimesheetEntry';
import {TimesheetOutputMapper} from '../../timesheet/_models/TimesheetOutputMapper';
import {ProjectOutputMapper} from '../../../enterprise/project/_models/ProjectOutputMapper';
import {SubprojectOutputMapper} from '../../../enterprise/subproject/_models/SubprojectOutputMapper';
import {WorkingMonthOutputMapper} from '../../../enterprise/working-month/_models/WorkingMonthOutputMapper';

export class TimesheetEntryMapper {
  static map(timesheetEntryOutputModel: TimesheetEntryOutputModel): TimesheetEntry {
    const timesheetEntry = new TimesheetEntry();
    timesheetEntry.date = timesheetEntryOutputModel.date ? timesheetEntryOutputModel.date : null;
    timesheetEntry.hours = timesheetEntryOutputModel.hours ? timesheetEntryOutputModel.hours : null;
    timesheetEntry.description = timesheetEntryOutputModel.description ? timesheetEntryOutputModel.description : null;
    timesheetEntry.status = timesheetEntryOutputModel.status ? timesheetEntryOutputModel.status : null;
    timesheetEntry.externalId = timesheetEntryOutputModel.externalId ? timesheetEntryOutputModel.externalId : null;
    timesheetEntry.timesheet = timesheetEntryOutputModel.timesheet ? TimesheetOutputMapper.map(timesheetEntryOutputModel.timesheet) : null;
    timesheetEntry.project = timesheetEntryOutputModel.project ? ProjectOutputMapper.map(timesheetEntryOutputModel.project) : null;
    timesheetEntry.subproject = timesheetEntryOutputModel.subproject ? SubprojectOutputMapper.map(timesheetEntryOutputModel.subproject) : null;
    timesheetEntry.name = timesheetEntryOutputModel.name ? timesheetEntryOutputModel.name : null;
    timesheetEntry.workingMonth = timesheetEntryOutputModel.workingMonth ? WorkingMonthOutputMapper.map(timesheetEntryOutputModel.workingMonth) : null;

    // custom for date: start
    timesheetEntry.date = timesheetEntryOutputModel.date ? timesheetEntryOutputModel.date.substr(0, 10) : null;
    // custom for date: end

    return timesheetEntry;
  }
}
