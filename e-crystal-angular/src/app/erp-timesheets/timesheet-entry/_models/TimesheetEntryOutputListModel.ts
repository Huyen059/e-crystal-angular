import {TimesheetEntryOutputModel} from './TimesheetEntryOutputModel';

export class TimesheetEntryOutputListModel {
  constructor(
    public timesheetEntries: TimesheetEntryOutputModel[] = []
  ) {
  }
}
