import {TimesheetReferenceModel} from '../../timesheet/_models/TimesheetReferenceModel';
import {ProjectReferenceModel} from '../../../enterprise/project/_models/ProjectReferenceModel';
import {SubprojectReferenceModel} from '../../../enterprise/subproject/_models/SubprojectReferenceModel';
import {WorkingMonthReferenceModel} from '../../../enterprise/working-month/_models/WorkingMonthReferenceModel';

export class TimesheetEntryOutputModel {
  date: string;
  hours: number;
  description: string;
  status: string;
  externalId: string;
  timesheet: TimesheetReferenceModel;
  project: ProjectReferenceModel;
  subproject: SubprojectReferenceModel;
  name: string;
  // custom
  workingMonth: WorkingMonthReferenceModel;
  numberOfExpenses: number;
}
