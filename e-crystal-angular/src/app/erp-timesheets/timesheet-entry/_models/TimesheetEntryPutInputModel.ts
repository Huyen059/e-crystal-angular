export class TimesheetEntryPutInputModel {
  constructor(
    public date: string = null,
    public hours: number = null,
    public description: string = null,
    public status: string = null,
    public externalId: string = null,
    public timesheet: string = null,
    public project: string = null,
    public subproject: string = null,
  ) {  }
}
