import { Component, OnInit } from '@angular/core';
import {TimesheetService} from '../_services/timesheet.service';

@Component({
  selector: 'app-timesheet-workspace',
  templateUrl: './timesheet-workspace.component.html',
  styleUrls: ['./timesheet-workspace.component.css']
})
export class TimesheetWorkspaceComponent implements OnInit {

  constructor(
    private timesheetService: TimesheetService
  ) { }

  ngOnInit(): void {
    this.timesheetService.resetFetchItemConditions();
    this.timesheetService.fetchItemConditions.sortBy = null;
    this.timesheetService.pageUrl = null;
  }

}
