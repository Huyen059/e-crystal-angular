import {AfterViewInit, Component, OnInit} from '@angular/core';
import {TimesheetService} from '../_services/timesheet.service';
import {EmployeeService} from '../../../enterprise/employee/_services/employee.service';
import {EmployeeOutputModel} from '../../../enterprise/employee/_models/EmployeeOutputModel';
import {Location} from '@angular/common';
import {FilterService} from '../../../filter/_services/filter.service';
import {Timesheet} from '../_models/Timesheet';
import {TimesheetPostInputMapper} from '../_models/TimesheetPostInputMapper';
import {Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, switchMap} from 'rxjs/operators';
import {AuthService} from '../../../_services/auth.service';

@Component({
  selector: 'app-timesheet-new',
  templateUrl: './timesheet-new.component.html',
  styleUrls: ['./timesheet-new.component.css']
})
export class TimesheetNewComponent implements OnInit, AfterViewInit {
  timesheet: Timesheet = new Timesheet();

  // for each linked field
  employees: EmployeeOutputModel[] = [];
  isEmployeeOptionsShown = false;

  isFilterMenuShown = false;

  // for dynamic search
  private searchTerm = new Subject<string>();

  constructor(
    private timesheetService: TimesheetService,
    private employeeService: EmployeeService,
    private filterService: FilterService,
    private location: Location,
    private authService: AuthService,
  ) {  }

  get currentUser() {
    return this.authService.currentUserValue;
  }

  // for each linked field
  get chosenEmployee(): EmployeeOutputModel {
    if (this.filterService.hasSelectedItem('enterprise.employee')) {
      this.timesheet.employee = this.filterService.currentElement().selectedItem.value;
      this.filterService.currentElement().selectedItem = null;
    }
    return this.timesheet.employee;
  }

  ngOnInit(): void {
    this.filterService.resetFilterData();
  }

  ngAfterViewInit(): void {
    this.cancelDialog();
  }

  private cancelDialog() {
    window.onclick = this.closeDropdownsOnClickOutside();
    window.onkeyup = this.closeDropdownsOnPressingEscape();
  }

  private closeDropdownsOnPressingEscape() {
    return e => {
      if (e.key === 'Escape') {
        this.isEmployeeOptionsShown = false;
      }
    };
  }

  private closeDropdownsOnClickOutside() {
    const employeeDropdown = document.querySelector('.employee-dropdown') as HTMLElement;
    return (e) => {
      if (!employeeDropdown.contains(e.target)) {
        this.isEmployeeOptionsShown = false;
      }
    };
  }

  /* ----- HTTP REQUESTS ----- */

  addTimesheet(): void {
    this.timesheetService.createTimesheet(TimesheetPostInputMapper.map(this.timesheet))
      .subscribe(() => {
        this.location.back();
      });
  }

  // for each linked field
  private getEmployees(): void {
    this.employeeService.getEmployees()
      .subscribe(data => this.employees = data._embedded.employees);
  }

  /* ----- DROPDOWN INPUT ----- */

  toggleDropdownOptions(elementName: string): void {
    // for each linked field
    if (elementName === 'enterprise.employee') {
      if (!this.isEmployeeOptionsShown) {
        this.getEmployees();
        this.isEmployeeOptionsShown = true;
      } else {
        this.isEmployeeOptionsShown = false;
      }
    }
  }

  private openDropdownOptions(elementName: string): void {
    if (elementName === 'enterprise.employee') {
      this.isEmployeeOptionsShown = true;
    }
  }

  private closeDropdownOptions(elementName: string): void {
    if (elementName === 'enterprise.employee') {
      this.isEmployeeOptionsShown = false;
    }
  }

  chooseADropdownOption(
    elementName: string,
    option:
      // for each linked field
      EmployeeOutputModel
  ): void {
    // for each linked field
    if (elementName === 'enterprise.employee') {
      this.isEmployeeOptionsShown = false;
      this.timesheet.employee = option as EmployeeOutputModel;
    }
  }

  showFilterComponentForCurrentField(elementName: string): void {
    this.closeDropdownOptions(elementName);
    this.isFilterMenuShown = true;
    this.filterService.resetFilterData();
    this.filterService.addFilter(elementName);
  }

  clearInputField(elementName: string) {
    // for each linked field
    if (elementName === 'enterprise.employee') {
      this.timesheet.employee = null;
    }
  }

  searchHandler(elementName: string, $event: KeyboardEvent) {
    this.openDropdownOptions(elementName);
    const input = ($event.target as HTMLInputElement).value;
    this.searchTerm.next(input);

    this.searchTerm.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      switchMap((term: string) => {
        if (elementName === 'enterprise.employee') {
          return this.employeeService.search(term);
        }

      })
    ).subscribe(data => {
      if (elementName === 'enterprise.employee') {
        this.employees = data._embedded.employees
      }
    });
  }
}
