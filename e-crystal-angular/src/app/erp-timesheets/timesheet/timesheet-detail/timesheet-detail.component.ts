import {AfterViewInit, Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';
import {TimesheetService} from '../_services/timesheet.service';
import {TimesheetOutputModel} from '../_models/TimesheetOutputModel';
import {NavigationService} from '../../../_services/navigation.service';
import {AuthService} from '../../../_services/auth.service';


@Component({
  selector: 'app-timesheet-detail',
  templateUrl: './timesheet-detail.component.html',
  styleUrls: ['./timesheet-detail.component.css']
})
export class TimesheetDetailComponent implements OnInit, AfterViewInit {
  numberOfDataChildren = 2;
  private firstTabPosition: number;
  private tabWidths: number[] = [];

  private selectedId: string;
  timesheet: TimesheetOutputModel;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private location: Location,
    private timesheetService: TimesheetService,
    private navigationService: NavigationService,
    private authService: AuthService,
  ) {  }

  get currentUser() {
    return this.authService.currentUserValue;
  }

  ngOnInit(): void {
    this.selectedId = this.route.snapshot.paramMap.get('externalId');
    this.getTimesheet();
    this.navigationService.parentElementFullName = 'erpTimesheets.timesheet';
    this.navigationService.parentExternalId = this.selectedId;

  }

  ngAfterViewInit(): void {
    if (this.numberOfDataChildren !== 0) {
      this.firstTabPosition = 1;
      this.showScrollIcons();
      this.setTabsWidth();
    }
  }


  /* ----- HTTP REQUESTS ----- */

  private getTimesheet(): void {
    this.timesheetService.getTimesheetByExternalId(this.selectedId)
      .subscribe(timesheet => this.timesheet = timesheet);
  }

  delete(externalId: string): void {
    this.timesheetService.delete(externalId)
      .subscribe(
        () => this.router.navigate(['/timesheets'])
      );
  }

  /* ----- NAVIGATION ----- */

  goToEditTimesheetForm(externalId: string): void {
    this.router.navigate(['/timesheets/edit', {externalId}]);
  }

  /* ----- TAB ----- */

  private setTabsWidth(): void {
    if (this.currentUser) {
    const tabLinks = document.getElementsByClassName('tab-links') as HTMLCollectionOf<HTMLElement>;
    for (let i = 0; i < tabLinks.length; i++) {
      tabLinks[i].style.width = tabLinks[i].offsetWidth + 'px';
      this.tabWidths.push(tabLinks[i].offsetWidth);
    }
    }
  }

  private showScrollIcons(): void {
    if (this.currentUser) {
    const tab = document.getElementsByClassName('tab')[0] as HTMLElement;
    const tabButtons = document.querySelector('.tab-buttons') as HTMLElement;
    const arrows = document.getElementsByClassName('arrow') as HTMLCollectionOf<HTMLElement>;
    if (tabButtons.offsetWidth < tab.offsetWidth) {
      for (let i = 0; i < arrows.length; i++) {
        arrows[i].style.display = 'none';
      }
    } else {
      this.updateArrowsDisplay();
    }
    }
  }

  private updateArrowsDisplay(): void {
    const leftArrow = document.querySelector('.leftArrow') as HTMLElement;
    const rightArrow = document.querySelector('.rightArrow') as HTMLElement;
    const tabLinks = document.getElementsByClassName('tab-links') as HTMLCollectionOf<HTMLElement>;
    if (this.firstTabPosition === 1) {
      leftArrow.style.display = 'none';
    } else if (this.firstTabPosition === tabLinks.length) {
      rightArrow.style.display = 'none';
    } else {
      leftArrow.style.display = 'block';
      rightArrow.style.display = 'block';
    }
  }

  showNextTab(): void {
    const tabButtons = document.querySelector('.tab-buttons') as HTMLElement;
    this.firstTabPosition++;
    tabButtons.style.marginLeft = '-' + this.calculateMargin();
    this.updateArrowsDisplay();
  }

  showPreviousTab(): void {
    const tabButtons = document.querySelector('.tab-buttons') as HTMLElement;
    this.firstTabPosition--;
    tabButtons.style.marginLeft = '-' + this.calculateMargin();
    this.updateArrowsDisplay();
  }

  private calculateMargin(): string {
    if (this.firstTabPosition === 1) {
      return '0';
    }

    let margin = 0;

    for (let i = 0; i < this.tabWidths.length; i++) {
      if (i > 0 && i < this.firstTabPosition) {
        margin += this.tabWidths[i - 1];
      }
    }

    return margin + 'px';
  }

}
