import {LinksModel} from '../../../_models/LinksModel';
import {PageModel} from '../../../_models/PageModel';
import {TimesheetOutputListModel} from './TimesheetOutputListModel';

export class PaginatedResponse {
  constructor(
    public _embedded: TimesheetOutputListModel = new TimesheetOutputListModel(),
    public _links: LinksModel = null,
    public _page: PageModel = null
  ) {
  }
}
