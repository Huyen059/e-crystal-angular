import {TimesheetReferenceModel} from './TimesheetReferenceModel';
import {TimesheetOutputModel} from './TimesheetOutputModel';

export class TimesheetOutputMapper {
  static map(timesheetReferenceModel: TimesheetReferenceModel): TimesheetOutputModel {
    const timesheetOutputModel = new TimesheetOutputModel();
    timesheetOutputModel.name = timesheetReferenceModel.name ? timesheetReferenceModel.name : null;
    timesheetOutputModel.externalId = timesheetReferenceModel.externalId ? timesheetReferenceModel.externalId : null;

    return timesheetOutputModel;
  }
}
