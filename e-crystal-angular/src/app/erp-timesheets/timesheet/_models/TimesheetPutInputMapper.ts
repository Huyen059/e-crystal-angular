import {Timesheet} from './Timesheet';
import {TimesheetPutInputModel} from './TimesheetPutInputModel';

export class TimesheetPutInputMapper {
  static map(timesheet: Timesheet): TimesheetPutInputModel {
    const timesheetPutInputModel = new TimesheetPutInputModel();
    timesheetPutInputModel.externalId = timesheet.externalId ? timesheet.externalId : null;
    timesheetPutInputModel.month = timesheet.month ? timesheet.month : null;
    timesheetPutInputModel.status = timesheet.status ? timesheet.status : null;
    timesheetPutInputModel.year = timesheet.year ? timesheet.year : null;
    timesheetPutInputModel.employee = timesheet.employee ? timesheet.employee.externalId : null;
    timesheetPutInputModel.name = timesheet.name ? timesheet.name : null;

    return timesheetPutInputModel;
  }

}
