import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import {PaginatedResponse} from '../_models/PaginatedResponse';
import {Timesheet} from '../_models/Timesheet';
import {TimesheetOutputModel} from '../_models/TimesheetOutputModel';
import {TimesheetPostInputModel} from '../_models/TimesheetPostInputModel';
import {TimesheetPutInputModel} from '../_models/TimesheetPutInputModel';
import {erpTimesheetsProperties} from '../../../_properties/erpTimesheetsProperties';
import {AlertService} from '../../../alert/_services/alert.service';
import {AuthService} from '../../../_services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class TimesheetService {

  private apiBaseUrl = erpTimesheetsProperties.baseUrl + '/timesheets';
  private httpReadOptions = {headers: { Authorization : `Basic ${this.authorizationData}` }};
  private httpWriteOptions = {headers: { Authorization : `Basic ${this.authorizationData}`, 'Content-Type': 'application/json' }};

  fields = {
    name: 'name',
    employee: 'employee',
    month: 'month',
    year: 'year',
    status: 'status',
  };

  filterOptions = [
    this.fields.employee,
    this.fields.month,
    this.fields.year,
    this.fields.status,
  ];

  defaultFilterOptions: string[] = [
    this.fields.month,
    this.fields.year,
  ];

  chosenFilterOptions: string[] = [
    this.fields.month,
    this.fields.year,
  ];

  fetchItemConditions: Timesheet = new Timesheet();
  private _hasFilter: boolean;
  pageUrl: string;

  constructor(
    private http: HttpClient,
    private alertService: AlertService,
    private authService: AuthService,
  ) { }

  get authorizationData(): string {
    return this.authService.currentUserValue ?
      this.authService.currentUserValue.authorizationData : '';
  }

  get hasFilter(): boolean {
    return this._hasFilter;
  }

  /* ----- READ ----- */

  getTimesheets(): Observable<PaginatedResponse> {
    if (this.authService.currentUserValue) {
      let url = this.apiBaseUrl;
      this.updateHasFilter();
      if (this.hasFilter || this.fetchItemConditions.sortBy) {
        url += '?' + TimesheetService.createQueryParameters(this.fetchItemConditions);
      }
      return this.http.get<PaginatedResponse>(url, this.httpReadOptions)
        .pipe(
          catchError(this.handleError('Get timesheets', new PaginatedResponse()))
        );
    }
    return of(new PaginatedResponse());
  }

  getTimesheetByExternalId(selectedId: string): Observable<TimesheetOutputModel> {
    if (this.authService.currentUserValue) {
      const url = this.apiBaseUrl + `/${selectedId}`;
      return this.http.get<TimesheetOutputModel>(url, this.httpReadOptions)
        .pipe(
          catchError(this.handleError('Get timesheet by external id', new TimesheetOutputModel()))
        );
    }
    return of(new TimesheetOutputModel());
  }

  getTimesheetsByPageUrl(url: string): Observable<PaginatedResponse> {
    if (this.authService.currentUserValue) {
      this.pageUrl = url;
      return this.http.get<PaginatedResponse>(url, this.httpReadOptions)
        .pipe(
          catchError(this.handleError('Get timesheets by page url', new PaginatedResponse()))
        );
    }
    return of(new PaginatedResponse());
  }

  search(term: string): Observable<PaginatedResponse> {
    if (this.authService.currentUserValue) {
      this.fetchItemConditions.name = term.trim();
      return this.getTimesheets();
    }
    return of(new PaginatedResponse());
  }

  /* ----- CREATE ----- */

  createTimesheet(timesheet: TimesheetPostInputModel): Observable<any> {
    if (this.authService.currentUserValue) {
      const url = this.apiBaseUrl;
      return this.http.post(url, timesheet, this.httpWriteOptions)
        .pipe(
          tap(_ => this.logSuccess('Timesheet created')),
          catchError(this.handleError('Get timesheet by external id'))
        );
    }
    this.handleError('Login to perform this operation', of(null));
  }

  /* ----- UPDATE ----- */

  saveTimesheet(timesheet: TimesheetPutInputModel): Observable<any> {
    if (this.authService.currentUserValue) {
      const url = this.apiBaseUrl + `/${timesheet.externalId}`;
      return this.http.put(url, timesheet, this.httpWriteOptions)
        .pipe(
          tap(_ => this.logSuccess('Timesheet updated')),
          catchError(this.handleError('Update timesheet'))
        );
    }
    this.handleError('Login to perform this operation', of(null));
  }

  /* ----- DELETE ----- */

  delete(externalId: string): Observable<any> {
    if (this.authService.currentUserValue) {
      const url = this.apiBaseUrl + `/${externalId}`;
      return this.http.delete(url, this.httpWriteOptions)
        .pipe(
          tap(_ => this.logSuccess('Timesheet deleted')),
          catchError(this.handleError('Delete timesheet'))
        );
    }
    this.handleError('Login to perform this operation', of(null));
  }

  /* ----- ERROR HANDLING ----- */

  private handleError<T>(operation = 'Operation', result?: T): (error: any) => Observable<T> {
    return (error: any): Observable<T> => {
      this.logError(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

  /* ----- LOGGING ----- */

  private logSuccess(message: string): void {
    this.alertService.success(`TimesheetService: ${message}`, { autoClose: true });
  }

  private logError(message: string): void {
    this.alertService.error(`TimesheetService: ${message}`, { autoClose: true });
  }

  /* ----- FILTER ----- */

  private static createQueryParameters(fetchItemConditions: Timesheet): string {
    const parameters: string[] = [];

    if (fetchItemConditions.employee) {
      parameters.push('employee=' + fetchItemConditions.employee.externalId);
    }

    if (fetchItemConditions.month) {
      parameters.push('month=' + fetchItemConditions.month);
    }

    if (fetchItemConditions.year) {
      parameters.push('year=' + fetchItemConditions.year);
    }

    if (fetchItemConditions.status) {
      parameters.push('status=' + fetchItemConditions.status);
    }

    if (fetchItemConditions.sortBy) {
      parameters.push('sortBy=' + fetchItemConditions.sortBy);
    }

    if (fetchItemConditions.isAscending !== null) {
      if (fetchItemConditions.isAscending) {
        parameters.push('isAscending=true');
      } else {
        parameters.push('isAscending=false');
      }
    }

    return parameters.join('&');
  }

  findIndexOfChosenFilterOption(fieldName: string): number {
    return this.chosenFilterOptions.findIndex(chosenOption => chosenOption === fieldName);
  }

  private resetDefaultFilterOptions(): void {
    this.chosenFilterOptions = this.defaultFilterOptions;
  }

  resetFilter(): void {
    this.filterOptions.forEach(option => {
      this.fetchItemConditions[option] = null;
    });
    this.resetDefaultFilterOptions();
    this._hasFilter = false;
  }

  resetFetchItemConditions(): void {
    this.fetchItemConditions = new Timesheet();
  }

  updateHasFilter(): void {
    this._hasFilter = false;
    for (const option of this.filterOptions) {
      if (this.fetchItemConditions[option] !== null) {
        this._hasFilter = true;
        break;
      }
    }
  }
}
