import {Component, OnInit} from '@angular/core';
import {TimesheetService} from '../_services/timesheet.service';
import {ActivatedRoute, Router} from '@angular/router';
import {TimesheetOutputModel} from '../_models/TimesheetOutputModel';
import {LinksModel} from '../../../_models/LinksModel';
import {PageModel} from '../../../_models/PageModel';
import {Timesheet} from '../_models/Timesheet';
import {FilterService} from '../../../filter/_services/filter.service';
import {PaginatedResponse} from '../_models/PaginatedResponse';
import {AuthService} from '../../../_services/auth.service';

@Component({
  selector: 'app-timesheet-list',
  templateUrl: './timesheet-list.component.html',
  styleUrls: ['./timesheet-list.component.css']
})
export class TimesheetListComponent implements OnInit {
  private elementFullName = 'erpTimesheets.timesheet';

  timesheets: TimesheetOutputModel[] = [];
  private links: LinksModel;
  page: PageModel;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private timesheetService: TimesheetService,
    private filterService: FilterService,
    private authService: AuthService,
  ) {  }

  get currentUser() {
    return this.authService.currentUserValue;
  }

  get fields(): any {
    return this.timesheetService.fields;
  }

  get fetchItemConditions(): Timesheet {
    return this.timesheetService.fetchItemConditions;
  }

  ngOnInit(): void {
    this.filterService.resetFilterData();
    this.filterService.addFilter(this.elementFullName);
    this.getTimesheetOnInit();
  }

  private getTimesheetOnInit(): void {
    if (this.timesheetService.pageUrl) {
      this.getTimesheetsByPageUrl(this.timesheetService.pageUrl);
    } else {
      this.getTimesheets();
    }
  }

  /* ----- HTTP REQUESTS -----*/

  private getTimesheets(): void {
    this.timesheetService.getTimesheets()
      .subscribe(
        (data: PaginatedResponse) => {
          this.extractData(data);
        }
      );
  }

  private getTimesheetsByPageUrl(url: string): void {
    this.timesheetService.getTimesheetsByPageUrl(url)
      .subscribe(
        (data: PaginatedResponse) => {
          this.extractData(data);
        }
      );
  }

  delete(externalId: string): void {
    this.timesheetService.delete(externalId)
      .subscribe(() => this.getTimesheetOnInit());
  }

  /* ----- NAVIGATION -----*/

  goToTimesheetDetailPage(externalId: string): void {
    this.router.navigate(['timesheets/' + externalId + '/timesheetentries']);
  }

  goToAddTimesheetForm(): void {
    this.router.navigate(['timesheets/new']);
  }

  goToEditTimesheetForm(externalId: string): void {
    this.router.navigate(['timesheets/edit', {externalId}]);
  }

  /* ----- PAGINATION -----*/

  displayFirstPageOfList(): void {
    this.getTimesheetsByPageUrl(this.links.first.href);
  }

  displayPreviousPageOfList(): void {
    if (this.links.prev) {
      this.getTimesheetsByPageUrl(this.links.prev.href);
    }
  }

  displayNextPageOfList(): void {
    if (this.links.next) {
      this.getTimesheetsByPageUrl(this.links.next.href);
    }
  }

  displayLastPageOfList(): void {
    this.getTimesheetsByPageUrl(this.links.last.href);
  }

  /* ----- FILTER ----- */

  /*  filter event catchers  */

  onFilter(dataFromApi: any): void {
    this.extractData(dataFromApi);
  }

  /* ----- SORT ----- */

  getSortCssClass(sortField: string): string {
    if (sortField !== this.timesheetService.fetchItemConditions.sortBy) {
      return 'fas fa-sort';
    }

    if (this.timesheetService.fetchItemConditions.isAscending) {
      return 'fas fa-sort-down';
    }

    return 'fas fa-sort-up';
  }

  enableSort(sortField: string): void {
    if (this.timesheetService.fetchItemConditions.sortBy === sortField) {
      this.switchSortDirection();
    } else {
      this.setSortField(sortField);
    }

    this.sort();
  }

  private setSortField(sortField: string): void {
    this.timesheetService.fetchItemConditions.sortBy = sortField;
    this.timesheetService.fetchItemConditions.isAscending = true;
  }

  private switchSortDirection(): void {
    this.timesheetService.fetchItemConditions.isAscending = !this.timesheetService.fetchItemConditions.isAscending;
  }

  private sort(): void {
    this.getTimesheets();
  }


  clearSort() {
    this.timesheetService.fetchItemConditions.sortBy = null;
    this.getTimesheets();
  }

  /* ----- MISC ----- */

  private extractData(data: PaginatedResponse): void {
    this.timesheets = data._embedded.timesheets;
    this.links = data._links;
    this.page = data._page;
    if (this.links) {
      this.timesheetService.pageUrl = this.links.self.href;
    }
  }
}
