import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ErpTimesheetsRoutingModule } from './erp-timesheets-routing.module';

import { FilterModule } from '../filter/filter.module';
import { EnterpriseModule } from '../enterprise/enterprise.module';

import { TimesheetWorkspaceComponent } from './timesheet/timesheet-workspace/timesheet-workspace.component';
import { TimesheetListComponent } from './timesheet/timesheet-list/timesheet-list.component';
import { TimesheetAsSublistComponent } from './timesheet/timesheet-as-sublist/timesheet-as-sublist.component';
import { TimesheetNewComponent } from './timesheet/timesheet-new/timesheet-new.component';
import { TimesheetEditComponent } from './timesheet/timesheet-edit/timesheet-edit.component';
import { TimesheetDetailComponent } from './timesheet/timesheet-detail/timesheet-detail.component';

import { TimesheetEntryWorkspaceComponent } from './timesheet-entry/timesheet-entry-workspace/timesheet-entry-workspace.component';
import { TimesheetEntryListComponent } from './timesheet-entry/timesheet-entry-list/timesheet-entry-list.component';
import { TimesheetEntryAsSublistComponent } from './timesheet-entry/timesheet-entry-as-sublist/timesheet-entry-as-sublist.component';
import { TimesheetEntryNewComponent } from './timesheet-entry/timesheet-entry-new/timesheet-entry-new.component';
import { TimesheetEntryEditComponent } from './timesheet-entry/timesheet-entry-edit/timesheet-entry-edit.component';
import { TimesheetEntryDetailComponent } from './timesheet-entry/timesheet-entry-detail/timesheet-entry-detail.component';

import { TimesheetRemarkWorkspaceComponent } from './timesheet-remark/timesheet-remark-workspace/timesheet-remark-workspace.component';
import { TimesheetRemarkListComponent } from './timesheet-remark/timesheet-remark-list/timesheet-remark-list.component';
import { TimesheetRemarkAsSublistComponent } from './timesheet-remark/timesheet-remark-as-sublist/timesheet-remark-as-sublist.component';
import { TimesheetRemarkNewComponent } from './timesheet-remark/timesheet-remark-new/timesheet-remark-new.component';
import { TimesheetRemarkEditComponent } from './timesheet-remark/timesheet-remark-edit/timesheet-remark-edit.component';

import { ExpenseWorkspaceComponent } from './expense/expense-workspace/expense-workspace.component';
import { ExpenseListComponent } from './expense/expense-list/expense-list.component';
import { ExpenseAsSublistComponent } from './expense/expense-as-sublist/expense-as-sublist.component';
import { ExpenseNewComponent } from './expense/expense-new/expense-new.component';
import { ExpenseEditComponent } from './expense/expense-edit/expense-edit.component';
import { ExpenseDetailComponent } from './expense/expense-detail/expense-detail.component';


@NgModule({
    declarations: [
        TimesheetWorkspaceComponent,
        TimesheetListComponent,
        TimesheetAsSublistComponent,
        TimesheetNewComponent,
        TimesheetEditComponent,
        TimesheetDetailComponent,

        TimesheetEntryWorkspaceComponent,
        TimesheetEntryListComponent,
        TimesheetEntryAsSublistComponent,
        TimesheetEntryNewComponent,
        TimesheetEntryEditComponent,
        TimesheetEntryDetailComponent,

        TimesheetRemarkWorkspaceComponent,
        TimesheetRemarkListComponent,
        TimesheetRemarkAsSublistComponent,
        TimesheetRemarkNewComponent,
        TimesheetRemarkEditComponent,

        ExpenseWorkspaceComponent,
        ExpenseListComponent,
        ExpenseAsSublistComponent,
        ExpenseNewComponent,
        ExpenseEditComponent,
        ExpenseDetailComponent],
  exports: [  ],
  imports: [
    CommonModule,
    FormsModule,
    FilterModule,
    EnterpriseModule,
    ErpTimesheetsRoutingModule,
  ]
})
export class ErpTimesheetsModule { }
