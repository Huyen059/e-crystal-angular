import {Component, Input, OnInit} from '@angular/core';
import {TtimesheetRemarkViewModel} from '../_models/TtimesheetRemarkViewModel';
import {LinksModel} from '../../../_models/LinksModel';
import {PageModel} from '../../../_models/PageModel';
import {ActivatedRoute, Router} from '@angular/router';
import {TimesheetRemarkService} from '../_services/timesheet-remark.service';
import {TimesheetOutputModel} from '../../timesheet/_models/TimesheetOutputModel';
import {TimesheetRemark} from '../_models/TimesheetRemark';

@Component({
  selector: 'app-timesheet-remark-as-sublist',
  templateUrl: './timesheet-remark-as-sublist.component.html',
  styleUrls: ['./timesheet-remark-as-sublist.component.css']
})
export class TimesheetRemarkAsSublistComponent implements OnInit {
  @Input() parentExternalId: string;
  @Input() parentElementFullName: string;

  timesheetRemarks: TtimesheetRemarkViewModel[];
  links: LinksModel;
  page: PageModel;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private timesheetRemarkService: TimesheetRemarkService,
  ) { }

  ngOnInit(): void {
    this.getTimesheetRemarks();
  }

  /* ----- HTTP REQUESTS -----*/

  getTimesheetRemarks(): void {
    this.setFilterOnInitValues();
    this.timesheetRemarkService.getTimesheetRemarks()
      .subscribe(
        data => {
          this.extractData(data);
        }
      );
  }

  getTimesheetRemarksByPageUrl(url: string): void {
    this.timesheetRemarkService.getTimesheetRemarksByPageUrl(url)
      .subscribe(
        data => {
          this.extractData(data);
        }
      );
  }

  delete(externalId: string): void {
    this.timesheetRemarkService.delete(externalId)
      .subscribe(() => this.getTimesheetRemarks());
  }

  /* ----- NAVIGATION -----*/

  goToTimesheetRemarkDetailPage(externalId: string): void {
    this.router.navigate(['timesheetremarks', externalId]);
  }

  goToAddTimesheetRemarkForm(): void {
    this.router.navigate(['timesheetremarks/new', {timesheet: this.parentExternalId}]);
  }

  goToEditTimesheetRemarkForm(externalId: string): void {
    this.router.navigate(['timesheetremarks/edit', {externalId}]);
  }

  /* ----- PAGINATION ----- */

  displayFirstPageOfList(): void {
    this.getTimesheetRemarksByPageUrl(this.links.first.href);
  }

  displayPreviousPageOfList(): void {
    this.getTimesheetRemarksByPageUrl(this.links.prev.href);
  }

  displayNextPageOfList(): void {
    this.getTimesheetRemarksByPageUrl(this.links.next.href);
  }

  displayLastPageOfList(): void {
    this.getTimesheetRemarksByPageUrl(this.links.last.href);
  }

  /* ----- FILTER ----- */

  /*  filter on init  */

  setFilterOnInitValues(): void {
    if (this.parentElementFullName === 'erpTimesheets.timesheet') {
      this.timesheetRemarkService.fetchItemConditions.timesheet = new TimesheetOutputModel();
      this.timesheetRemarkService.fetchItemConditions.timesheet.externalId = this.parentExternalId;
    }
  }

  /* ----- SORT ----- */

  getSortCssClass(sortField: string): string {
    if (sortField !== this.timesheetRemarkService.fetchItemConditions.sortBy) {
      return 'fas fa-sort';
    }

    if (this.timesheetRemarkService.fetchItemConditions.isAscending) {
      return 'fas fa-sort-down';
    }

    return 'fas fa-sort-up';
  }

  enableSort(sortField: string): void {
    if (this.timesheetRemarkService.fetchItemConditions.sortBy === sortField) {
      this.switchSortDirection();
    } else {
      this.setSortField(sortField);
    }

    this.sort();
  }

  setSortField(sortField: string): void {
    this.timesheetRemarkService.fetchItemConditions.sortBy = sortField;
    this.timesheetRemarkService.fetchItemConditions.isAscending = true;
  }

  switchSortDirection(): void {
    this.timesheetRemarkService.fetchItemConditions.isAscending = !this.timesheetRemarkService.fetchItemConditions.isAscending;
  }

  sort(): void {
    this.getTimesheetRemarks();
  }

  /* ----- GETTER ----- */

  get fields(): any {
    return this.timesheetRemarkService.fields;
  }

  get fetchItemConditions(): TimesheetRemark {
    return this.timesheetRemarkService.fetchItemConditions;
  }

  /* ----- MISC  -----*/

  extractData(data): void {
    this.timesheetRemarks = data._embedded.timesheetRemarks;
    this.links = data._links;
    this.page = data._page;
  }
}
