import { Injectable } from '@angular/core';
import {authorizationData} from '../../../../../credential';
import {HttpClient} from '@angular/common/http';
import {MessageService} from '../../../_services/message.service';
import {Observable, of} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import {TtimesheetRemarkViewModel} from '../_models/TtimesheetRemarkViewModel';
import {TimesheetRemark} from '../_models/TimesheetRemark';
import {TimesheetRemarkInputModel} from '../_models/TimesheetRemarkInputModel';
import {erpTimesheetsProperties} from '../../../_properties/erpTimesheetsProperties';

@Injectable({
  providedIn: 'root'
})
export class TimesheetRemarkService {

  private apiBaseUrl = erpTimesheetsProperties.baseUrl + '/timesheetremarks';
  private httpReadOptions = {headers: { Authorization : `Basic ${authorizationData}` }};
  private httpWriteOptions = {headers: { Authorization : `Basic ${authorizationData}`, 'Content-Type': 'application/json' }};

  fields = {
    timesheet: 'timesheet',
    remark: 'remark',
  };

  filterOptions = [
    this.fields.timesheet,
    this.fields.remark,
  ];


  defaultFilterOptions: string[] = [
    this.fields.timesheet,
  ];

  chosenFilterOptions: string[] = [
    this.fields.timesheet,
  ];

  fetchItemConditions: TimesheetRemark = new TimesheetRemark();
  private _hasFilter: boolean;

  constructor(
    private http: HttpClient,
    private messageService: MessageService,
  ) { }

  // ----- READ ----- //

  getTimesheetRemarks(): Observable<any> {
    let url = this.apiBaseUrl;
    this.updateHasFilter();
    if (this.hasFilter) {
      url += '?' + this.createQueryParameters(this.fetchItemConditions);
    }
    return this.http.get(url, this.httpReadOptions)
      .pipe(
        tap(_ => this.log('fetched timesheet remarks')),
        catchError(this.handleError('Get timesheet remarks', []))
      );
  }

  getTimesheetRemarkByExternalId(selectedId: string): Observable<any> {
    const url = this.apiBaseUrl + `/${selectedId}`;
    return this.http.get<TtimesheetRemarkViewModel>(url, this.httpReadOptions)
      .pipe(
        tap(_ => this.log('fetched timesheet remark with external id = ' + selectedId)),
        catchError(this.handleError('Get timesheet remark by external id'))
      );
  }

  getTimesheetRemarksByPageUrl(url: string): Observable<any> {
    return this.http.get(url, this.httpReadOptions)
      .pipe(
        tap(_ => this.log('fetched timesheet remarks by page url')),
        catchError(this.handleError('Get timesheet remarks by page url', []))
      );
  }

  // ----- CREATE ----- //

  createTimesheetRemark(timesheetRemark: TimesheetRemarkInputModel): Observable<any> {
    const url = this.apiBaseUrl;
    return this.http.post(url, timesheetRemark, this.httpWriteOptions)
      .pipe(
        tap((newTimesheetRemark: TtimesheetRemarkViewModel) =>
          this.log('create new timesheet remark with external id = ' +
            newTimesheetRemark.externalId)),
        catchError(this.handleError('Get timesheet remark by external id'))
      );
  }

  // ----- UPDATE ----- //

  saveTimesheetRemark(timesheetRemark: TimesheetRemarkInputModel): Observable<any> {
    const url = this.apiBaseUrl + `/${timesheetRemark.externalId}`;
    return this.http.put(url, timesheetRemark, this.httpWriteOptions)
      .pipe(
        tap(_ => this.log('update timesheet remark')),
        catchError(this.handleError('Update timesheet remark'))
      );
  }

  // ----- DELETE ----- //

  delete(externalId: string): Observable<any> {
    const url = this.apiBaseUrl + `/${externalId}`;
    return this.http.delete(url, this.httpWriteOptions)
      .pipe(
        tap(_ => this.log('delete timesheet remark with external id = ' + externalId)),
        catchError(this.handleError('Delete timesheet remark'))
      );
  }

  // ----- ERROR HANDLING ----- //

  private handleError<T>(operation = 'operation', result?: T): (error: any) => Observable<T> {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  // ----- LOGGING ----- //

  private log(message: string): void {
    this.messageService.add(`TimesheetRemarkService: ${message}`);
  }

  // ----- FILTER ----- //

  createQueryParameters(fetchItemConditions: TimesheetRemark): string {
    const parameters: string[] = [];

    if (fetchItemConditions.timesheet) {
      parameters.push('timesheet=' + fetchItemConditions.timesheet.externalId);
    }

    if (fetchItemConditions.remark) {
      parameters.push('remark=' + fetchItemConditions.remark);
    }

    if (fetchItemConditions.sortBy) {
      parameters.push('sortBy=' + fetchItemConditions.sortBy);
    }

    if (fetchItemConditions.isAscending !== null) {
      if (fetchItemConditions.isAscending) {
        parameters.push('isAscending=true');
      } else {
        parameters.push('isAscending=false');
      }
    }

    return parameters.join('&');
  }

  findIndexOfChosenFilterOption(fieldName: string): number {
    return this.chosenFilterOptions.findIndex(chosenOption => chosenOption === fieldName);
  }

  isFilterOptionChosen(fieldName: string): boolean {
    return this.findIndexOfChosenFilterOption(fieldName) !== -1;
  }

  resetDefaultFilterOptions(): void {
    this.chosenFilterOptions = this.defaultFilterOptions;
  }

  resetFilter(): void {
    this.filterOptions.forEach(option => {
      this.fetchItemConditions[option] = null;
    });
    this.resetDefaultFilterOptions();
    this._hasFilter = false;
  }

  resetFetchItemConditions(): void {
    this.fetchItemConditions = new TimesheetRemark();
  }

  updateHasFilter(): void {
    this._hasFilter = false;
    for (const option of this.filterOptions) {
      if (this.fetchItemConditions[option] !== null) {
        this._hasFilter = true;
        break
      }
    }
  }

  get hasFilter(): boolean {
    return this._hasFilter;
  }
}
