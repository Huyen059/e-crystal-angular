export class TimesheetRemarkInputModel {
  constructor(
    public remark: string = null,
    public externalId: string = null,
    public timesheet: string = null,
  ) {  }

}
