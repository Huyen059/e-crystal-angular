import {TimesheetReferenceModel} from '../../timesheet/_models/TimesheetReferenceModel';

export class TtimesheetRemarkViewModel {
  remark: string;
  externalId: string;
  timesheet: TimesheetReferenceModel;
}
