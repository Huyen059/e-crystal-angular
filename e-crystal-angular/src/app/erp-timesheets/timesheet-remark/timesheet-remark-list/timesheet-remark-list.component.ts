import {Component, OnInit} from '@angular/core';
import {LinksModel} from '../../../_models/LinksModel';
import {PageModel} from '../../../_models/PageModel';
import {ActivatedRoute, Router} from '@angular/router';
import {TimesheetRemark} from '../_models/TimesheetRemark';
import {TtimesheetRemarkViewModel} from '../_models/TtimesheetRemarkViewModel';
import {TimesheetRemarkService} from '../_services/timesheet-remark.service';
import {FilterService} from '../../../filter/_services/filter.service';

@Component({
  selector: 'app-timesheet-remark-list',
  templateUrl: './timesheet-remark-list.component.html',
  styleUrls: ['./timesheet-remark-list.component.css']
})
export class TimesheetRemarkListComponent implements OnInit {
  elementFullName = 'erpTimesheets.timesheetRemark';

  timesheetRemarks: TtimesheetRemarkViewModel[];
  links: LinksModel;
  page: PageModel;
  isFilterMenuShown = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private timesheetRemarkService: TimesheetRemarkService,
    private filterService: FilterService
  ) { }

  ngOnInit(): void {
    this.filterService.resetFilterData();
    this.timesheetRemarkService.resetFetchItemConditions();
    this.getTimesheetRemarks();
  }

  /* ----- HTTP REQUESTS -----*/

  getTimesheetRemarks(): void {
    this.timesheetRemarkService.getTimesheetRemarks()
      .subscribe(
        data => {
          this.extractData(data);
        }
      );
  }

  getTimesheetRemarksByPageUrl(url: string): void {
    this.timesheetRemarkService.getTimesheetRemarksByPageUrl(url)
      .subscribe(
        data => {
          this.extractData(data);
        }
      );
  }

  delete(externalId: string): void {
    this.timesheetRemarkService.delete(externalId)
      .subscribe(() => this.getTimesheetRemarks());
  }

  /* ----- NAVIGATION -----*/

  goToTimesheetRemarkDetailPage(externalId: string): void {
    this.router.navigate(['timesheetremarks', externalId]);
  }

  goToAddTimesheetRemarkForm(): void {
    this.router.navigate(['timesheetremarks/new']);
  }

  goToEditTimesheetRemarkForm(externalId: string): void {
    this.router.navigate(['timesheetremarks/edit', {externalId}]);
  }

  /* ----- PAGINATION ----- */

  displayFirstPageOfList(): void {
    this.getTimesheetRemarksByPageUrl(this.links.first.href);
  }

  displayPreviousPageOfList(): void {
    this.getTimesheetRemarksByPageUrl(this.links.prev.href);
  }

  displayNextPageOfList(): void {
    this.getTimesheetRemarksByPageUrl(this.links.next.href);
  }

  displayLastPageOfList(): void {
    this.getTimesheetRemarksByPageUrl(this.links.last.href);
  }

  /* ----- FILTER ----- */

  /*  clear filter  */

  clearFilters(): void {
    this.timesheetRemarkService.resetFilter();
    this.getTimesheetRemarks();
  }

  /*  toggle filter menu */

  toggleFilterMenu(): void {
    this.isFilterMenuShown = !this.isFilterMenuShown;
    if (this.isFilterMenuShown) {
      this.filterService.resetFilterData();
      this.filterService.addFilter(this.elementFullName);
    }
  }

  /*  filter event catchers  */

  onFilter(dataFromApi: any): void {
    this.extractData(dataFromApi);
  }

  /* ----- SORT ----- */

  getSortCssClass(sortField: string): string {
    if (sortField !== this.timesheetRemarkService.fetchItemConditions.sortBy) {
      return 'fas fa-sort';
    }

    if (this.timesheetRemarkService.fetchItemConditions.isAscending) {
      return 'fas fa-sort-down';
    }

    return 'fas fa-sort-up';
  }

  enableSort(sortField: string): void {
    if (this.timesheetRemarkService.fetchItemConditions.sortBy === sortField) {
      this.switchSortDirection();
    } else {
      this.setSortField(sortField);
    }

    this.sort();
  }

  setSortField(sortField: string): void {
    this.timesheetRemarkService.fetchItemConditions.sortBy = sortField;
    this.timesheetRemarkService.fetchItemConditions.isAscending = true;
  }

  switchSortDirection(): void {
    this.timesheetRemarkService.fetchItemConditions.isAscending = !this.timesheetRemarkService.fetchItemConditions.isAscending;
  }

  sort(): void {
    this.getTimesheetRemarks();
  }

  /* ----- GETTER ----- */

  get fields(): any {
    return this.timesheetRemarkService.fields;
  }

  get fetchItemConditions(): TimesheetRemark {
    return this.timesheetRemarkService.fetchItemConditions;
  }

  get hasFilter(): boolean {
    return this.timesheetRemarkService.hasFilter;
  }

  /* ----- MISC  -----*/

  extractData(data): void {
    this.timesheetRemarks = data._embedded.timesheetRemarks;
    this.links = data._links;
    this.page = data._page;
  }
}
