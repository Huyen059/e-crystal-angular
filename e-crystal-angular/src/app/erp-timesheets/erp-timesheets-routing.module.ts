import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {TimesheetWorkspaceComponent} from './timesheet/timesheet-workspace/timesheet-workspace.component';
import {TimesheetListComponent} from './timesheet/timesheet-list/timesheet-list.component';
import {TimesheetNewComponent} from './timesheet/timesheet-new/timesheet-new.component';
import {TimesheetEditComponent} from './timesheet/timesheet-edit/timesheet-edit.component';
import {TimesheetDetailComponent} from './timesheet/timesheet-detail/timesheet-detail.component';

import {TimesheetEntryAsSublistComponent} from './timesheet-entry/timesheet-entry-as-sublist/timesheet-entry-as-sublist.component';
import {TimesheetRemarkAsSublistComponent} from './timesheet-remark/timesheet-remark-as-sublist/timesheet-remark-as-sublist.component';

import {TimesheetEntryWorkspaceComponent} from './timesheet-entry/timesheet-entry-workspace/timesheet-entry-workspace.component';
import {TimesheetEntryListComponent} from './timesheet-entry/timesheet-entry-list/timesheet-entry-list.component';
import {TimesheetEntryNewComponent} from './timesheet-entry/timesheet-entry-new/timesheet-entry-new.component';
import {TimesheetEntryEditComponent} from './timesheet-entry/timesheet-entry-edit/timesheet-entry-edit.component';
import {TimesheetEntryDetailComponent} from './timesheet-entry/timesheet-entry-detail/timesheet-entry-detail.component';

import {ExpenseAsSublistComponent} from './expense/expense-as-sublist/expense-as-sublist.component';

import {TimesheetRemarkWorkspaceComponent} from './timesheet-remark/timesheet-remark-workspace/timesheet-remark-workspace.component';
import {TimesheetRemarkNewComponent} from './timesheet-remark/timesheet-remark-new/timesheet-remark-new.component';
import {TimesheetRemarkEditComponent} from './timesheet-remark/timesheet-remark-edit/timesheet-remark-edit.component';

import {ExpenseWorkspaceComponent} from './expense/expense-workspace/expense-workspace.component';
import {ExpenseNewComponent} from './expense/expense-new/expense-new.component';
import {ExpenseEditComponent} from './expense/expense-edit/expense-edit.component';

const routes: Routes = [
  {
    path: 'timesheets',
    component: TimesheetWorkspaceComponent,
    data: {
      name: 'Timesheets'
    },
    children: [
      {
        path: '',
        component: TimesheetListComponent,
        data: {
          name: ''
        },
      },
      {
        path: 'new',
        component: TimesheetNewComponent,
        data: {
          name: 'New'
        },
      },
      {
        path: 'edit',
        component: TimesheetEditComponent,
        data: {
          name: 'Edit'
        },
      },
      {
        path: ':externalId',
        component: TimesheetDetailComponent,
        data: {
          name: 'Timesheet Detail'
        },
        children: [
          {
            path: 'timesheetentries',
            component: TimesheetEntryWorkspaceComponent,
            data: {
              name: 'Timesheet Entries'
            },
            children: [
              {
                path: '',
                component: TimesheetEntryAsSublistComponent,
                data: {
                  name: ''
                },
              },
              {
                path: 'new',
                component: TimesheetEntryNewComponent,
                data: {
                  name: 'New'
                },
              },
              {
                path: 'edit',
                component: TimesheetEntryEditComponent,
                data: {
                  name: 'Edit'
                },
              },
            ]
          },
          {
            path: 'timesheetremarks',
            component: TimesheetRemarkWorkspaceComponent,
            data: {
              name: 'Timesheet Remarks'
            },
            children: [
              {
                path: '',
                component: TimesheetRemarkAsSublistComponent,
                data: {
                  name: ''
                },
              },
              {
                path: 'new',
                component: TimesheetRemarkNewComponent,
                data: {
                  name: 'New'
                },
              },
              {
                path: 'edit',
                component: TimesheetRemarkEditComponent,
                data: {
                  name: 'Edit'
                },
              },
            ]
          },
        ]
      },
    ]
  },

  {
    path: 'timesheetentries',
    component: TimesheetEntryWorkspaceComponent,
    data: {
      name: 'Timesheet Entries'
    },
    children: [
      {
        path: '',
        component: TimesheetEntryListComponent,
        data: {
          name: ''
        },
      },
      {
        path: 'new',
        component: TimesheetEntryNewComponent,
        data: {
          name: 'New'
        },
      },
      {
        path: 'edit',
        component: TimesheetEntryEditComponent,
        data: {
          name: 'Edit'
        },
      },
      {
        path: ':externalId',
        component: TimesheetEntryDetailComponent,
        data: {
          name: 'Timesheet Entry Detail'
        },
        children: [
          {
            path: 'expenses',
            component: ExpenseWorkspaceComponent,
            data: {
              name: 'Expenses'
            },
            children: [
              {
                path: '',
                component: ExpenseAsSublistComponent,
                data: {
                  name: ''
                },
              },
              {
                path: 'new',
                component: ExpenseNewComponent,
                data: {
                  name: 'New'
                },
              },
              {
                path: 'edit',
                component: ExpenseEditComponent,
                data: {
                  name: 'Edit'
                },
              },
            ]
          },
        ]
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ErpTimesheetsRoutingModule {
}
