export const elementDisplayNames = {
  'erpTimesheets.timesheet': 'Timesheet',
  'erpTimesheets.timesheetEntry': 'Timesheet Entry',
  'erpTimesheets.timesheetRemark': 'Timesheet Remark',
  'enterprise.employee': 'Employee',
  'enterprise.employeeType': 'Employee Type',
  'enterprise.sourcer': 'Sourcer',
}
