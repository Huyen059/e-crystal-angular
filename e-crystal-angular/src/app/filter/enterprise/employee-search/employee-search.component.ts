import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {Subject} from 'rxjs';
import {EmployeeService} from '../../../enterprise/employee/_services/employee.service';
import {debounceTime, distinctUntilChanged, switchMap} from 'rxjs/operators';

@Component({
  selector: 'app-employee-search',
  templateUrl: './employee-search.component.html',
  styleUrls: ['./employee-search.component.css']
})
export class EmployeeSearchComponent implements OnInit {
  private searchTerm = new Subject<string>();

  @Output() searchResult = new EventEmitter<any>();

  constructor(
    private employeeService: EmployeeService
  ) { }

  ngOnInit(): void {
    this.searchTerm.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      switchMap((term: string) => {
        return this.employeeService.search(term);
      })
    )
      .subscribe(data => this.searchResult.emit(data));
  }

  search(term: string): void {
    this.searchTerm.next(term);
  }
}
