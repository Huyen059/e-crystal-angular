import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {LinksModel} from '../../../_models/LinksModel';
import {PageModel} from '../../../_models/PageModel';
import {EmployeeTypeViewModel} from '../../../enterprise/employee-type/_models/EmployeeTypeViewModel';
import {EmployeeTypeService} from '../../../enterprise/employee-type/_services/employee-type.service';
import {FilterService} from '../../_services/filter.service';

@Component({
  selector: 'app-employee-type-filter-list',
  templateUrl: './employee-type-filter-list.component.html',
  styleUrls: ['./employee-type-filter-list.component.css']
})
export class EmployeeTypeFilterListComponent implements OnInit {

  @Input() employeeTypes: EmployeeTypeViewModel[];
  links: LinksModel;
  page: PageModel;

  @Output() selectedEmployeeType = new EventEmitter<EmployeeTypeViewModel>();

  constructor(
    private employeeTypeService: EmployeeTypeService,
    private filterService: FilterService
  ) { }

  ngOnInit(): void {
    this.getEmployeeTypes();
  }

  /* ----- HTTP REQUESTS -----*/

  getEmployeeTypes(): void {
    this.employeeTypeService.getEmployeeTypes()
      .subscribe(
        data => {
          this.extractData(data);
        }
      );
  }

  getEmployeeTypesByPageUrl(url: string): void {
    this.employeeTypeService.getEmployeeTypesByPageUrl(url)
      .subscribe(
        data => {
          this.extractData(data);
        }
      );
  }

  getEmployeeTypesWithFilter(): void {
    this.employeeTypeService.getEmployeeTypesWithFilter()
      .subscribe(
        data => {
          this.extractData(data);
        }
      );
  }

  /* ----- SELECT ITEM ----- */

  selectEmployeeType(employeeType: EmployeeTypeViewModel): void {
    this.selectedEmployeeType.emit(employeeType);
  }

  /* ----- PAGINATION -----*/

  displayFirstPageOfList(): void {
    this.getEmployeeTypesByPageUrl(this.links.first.href);
  }

  displayPreviousPageOfList(): void {
    if (this.links.prev) {
      this.getEmployeeTypesByPageUrl(this.links.prev.href);
    }
  }

  displayNextPageOfList(): void {
    if (this.links.next.href) {
      this.getEmployeeTypesByPageUrl(this.links.next.href);
    }
  }

  displayLastPageOfList(): void {
    this.getEmployeeTypesByPageUrl(this.links.last.href);
  }

  /* ----- MISC ----- */

  extractData(data): void {
    this.employeeTypes = data._embedded.employeeTypes;
    this.links = data._links;
    this.page = data._page;
  }
}
