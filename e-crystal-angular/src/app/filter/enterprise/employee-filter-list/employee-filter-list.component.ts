import {ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {EmployeeOutputModel} from '../../../enterprise/employee/_models/EmployeeOutputModel';
import {LinksModel} from '../../../_models/LinksModel';
import {PageModel} from '../../../_models/PageModel';
import {EmployeeService} from '../../../enterprise/employee/_services/employee.service';

@Component({
  selector: 'app-employee-filter-list',
  templateUrl: './employee-filter-list.component.html',
  styleUrls: ['./employee-filter-list.component.css']
})
export class EmployeeFilterListComponent implements OnChanges {
  @Input() employeeApiData: any;
  @Output() selectedEmployee = new EventEmitter<EmployeeOutputModel>();

  employees: EmployeeOutputModel[];
  links: LinksModel;
  page: PageModel;

  constructor(
    private employeeService: EmployeeService,
    private changeDetectorRef: ChangeDetectorRef
  ) { }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.employeeApiData) {
      this.extractData(changes.employeeApiData.currentValue);
    }
    this.changeDetectorRef.detectChanges();
  }

  /* ----- HTTP REQUESTS -----*/

  getEmployeesByPageUrl(url: string): void {
    this.employeeService.getEmployeesByPageUrl(url)
      .subscribe(
        data => {
          this.extractData(data);
        }
      );
  }

  /* ----- SELECT ITEM ----- */

  selectEmployee(employee: EmployeeOutputModel): void {
    this.selectedEmployee.emit(employee);
  }

  /* ----- PAGINATION -----*/

  displayFirstPageOfList(): void {
    this.getEmployeesByPageUrl(this.links.first.href);
  }

  displayPreviousPageOfList(): void {
    if (this.links.prev) {
      this.getEmployeesByPageUrl(this.links.prev.href);
    }
  }

  displayNextPageOfList(): void {
    if (this.links.next.href) {
      this.getEmployeesByPageUrl(this.links.next.href);
    }
  }

  displayLastPageOfList(): void {
    this.getEmployeesByPageUrl(this.links.last.href);
  }

  /* ----- MISC -----*/

  extractData(data): void {
    this.employees = data._embedded.employees;
    this.links = data._links;
    this.page = data._page;
  }
}
