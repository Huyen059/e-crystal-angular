import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {EmployeeOutputModel} from '../../../enterprise/employee/_models/EmployeeOutputModel';
import {EmployeeTypeViewModel} from '../../../enterprise/employee-type/_models/EmployeeTypeViewModel';
import {EmployeeService} from '../../../enterprise/employee/_services/employee.service';
import {Employee} from '../../../enterprise/employee/_models/Employee';
import {FilterService} from '../../_services/filter.service';
import {EmployeeTypeService} from '../../../enterprise/employee-type/_services/employee-type.service';
import {SourcerViewModel} from '../../../enterprise/sourcer/_models/SourcerViewModel';
import {SourcerService} from '../../../enterprise/sourcer/_services/sourcer.service';

@Component({
  selector: 'app-employee-filter',
  templateUrl: './employee-filter.component.html',
  styleUrls: ['./employee-filter.component.css']
})
export class EmployeeFilterComponent implements OnInit {

  @Input() showListForFirstFilter: boolean;
  // filter result, use in filter-list component
  employeeApiData: any;

  // dropdown manipulation
  employeeTypes: EmployeeTypeViewModel[] = [];
  isEmployeeTypeOptionsShown = false;
  sourcers: SourcerViewModel[] = [];
  isSourcerOptionsShown = false;

  @Output() filterResult = new EventEmitter<any>();
  @Output() selectedEmployee = new EventEmitter<EmployeeOutputModel>();

  constructor(
    private employeeService: EmployeeService,
    private employeeTypeService: EmployeeTypeService,
    private sourcerService: SourcerService,
    private filterService: FilterService,
  ) {  }

  ngOnInit(): void {
    if (this.isFilterListShown || this.showListForFirstFilter) {
      this.getEmployees();
    }
  }

  /* ----- HTTP REQUESTS -----*/

  getEmployees(): void {
    this.employeeService.getEmployees()
      .subscribe(data => {
        this.employeeApiData = data;
        this.filterResult.emit(data);
      });
  }

  getEmployeeTypes(): void {
    this.employeeTypeService.getEmployeeTypes()
      .subscribe(data => this.employeeTypes = data._embedded.employeeTypes);
  }

  getSourcers(): void {
    this.sourcerService.getSourcers()
      .subscribe(data => this.sourcers = data._embedded.sourcers);
  }

  /* ----- ACTIONS -----*/

  applyFilter(): void {
    this.getEmployees();
    this.employeeService.updateHasFilter();
  }

  clearFilters(): void {
    this.employeeService.resetFilter();
    this.getEmployees();
  }

  chooseFilterOption(option: string): void {
    const index = this.findIndexOfChosenFilterOption(option);
    if (index !== -1) {
      this.employeeService.chosenFilterOptions.splice(index, 1);
      this.employeeService.fetchItemConditions[option] = null;
    } else {
      this.employeeService.chosenFilterOptions.push(option);
    }
  }

  /* ----- DROPDOWN INPUT ----- */
  /*  employee type  */
  toggleEmployeeTypeOptions(): void {
    if (!this.isEmployeeTypeOptionsShown) {
      this.getEmployeeTypes();
      this.isEmployeeTypeOptionsShown = true;
    } else {
      this.isEmployeeTypeOptionsShown = false;
    }
  }

  chooseThisEmployeeType(employeeType: EmployeeTypeViewModel): void {
    this.isEmployeeTypeOptionsShown = false;
    this.employeeService.fetchItemConditions.employeeType = employeeType;
  }

  /*  sourcer  */
  toggleSourcerOptions(): void {
    if (!this.isSourcerOptionsShown) {
      this.getSourcers();
      this.isSourcerOptionsShown = true;
    } else {
      this.isSourcerOptionsShown = false;
    }
  }

  chooseThisSourcer(sourcer: SourcerViewModel): void {
    this.isSourcerOptionsShown = false;
    this.employeeService.fetchItemConditions.sourcer = sourcer;
  }

  /* ----- MISC -----*/

  resetFilterData(): void {
    this.employeeService.resetFilter();
  }

  isFilterOptionChosen(fieldName: string): boolean {
    return this.findIndexOfChosenFilterOption(fieldName) !== -1;
  }

  findIndexOfChosenFilterOption(fieldName: string): number {
    return this.employeeService.findIndexOfChosenFilterOption(fieldName);
  }

  /* ----- GETTER ----- */

  get isFilterListShown(): boolean {
    return this.filterService.isFilterListShown();
  }

  get fields(): {[key: string]: string} {
    return this.employeeService.fields;
  }

  get filterOptions(): string[] {
    return this.employeeService.filterOptions;
  }

  get fetchItemConditions(): Employee {
    return  this.employeeService.fetchItemConditions;
  }

  get chosenFilterOptions(): string[] {
    return this.employeeService.chosenFilterOptions;
  }

  /*  Each dropdown in the filter form of this component will need the following function  */
  get chosenEmployeeType(): EmployeeTypeViewModel {
    let elementFullName = 'enterprise.employeeType';
    if (this.filterService.hasSelectedField(elementFullName)) {
      this.employeeService.fetchItemConditions.employeeType = this.filterService.currentElement().selectedField.value;
      this.filterService.currentElement().selectedField = null;
    }
    return this.employeeService.fetchItemConditions.employeeType;
  }

  /*  Each dropdown in the filter form of this component will need the following function  */
  get chosenSourcer(): SourcerViewModel {
    let elementFullName = 'enterprise.sourcer';
    if (this.filterService.hasSelectedField(elementFullName)) {
      this.employeeService.fetchItemConditions.sourcer = this.filterService.currentElement().selectedField.value;
      this.filterService.currentElement().selectedField = null;
    }
    return this.employeeService.fetchItemConditions.sourcer;
  }

  /* ----- SWITCH FILTER ----- */

  /*  Each dropdown in the filter form of this component will need the following function  */
  showEmployeeTypeFilterComponent(): void {
    this.filterService.addFilter('enterprise.employeeType');
  }

  /*  Each dropdown in the filter form of this component will need the following function  */
  showSourcerFilterComponent(): void {
    this.filterService.addFilter('enterprise.sourcer');
  }


  /* ----- SELECT ITEM ----- */

  onSelectEmployee(employee: EmployeeOutputModel): void {
    this.filterService.onSelectItem(employee);
  }

  /* ----- SEARCH EVENT CATCHER ----- */

  onSearch(data: any): void {
    this.employeeApiData = data;
  }
}
