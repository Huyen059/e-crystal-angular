import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FilterService} from '../../_services/filter.service';
import {EmployeeTypeViewModel} from '../../../enterprise/employee-type/_models/EmployeeTypeViewModel';
import {EmployeeTypeService} from '../../../enterprise/employee-type/_services/employee-type.service';
import {EmployeeType} from '../../../enterprise/employee-type/_models/EmployeeType';


@Component({
  selector: 'app-employee-type-filter',
  templateUrl: './employee-type-filter.component.html',
  styleUrls: ['./employee-type-filter.component.css']
})
export class EmployeeTypeFilterComponent implements OnInit {

  @Input() showListForFirstFilter: boolean;
  // filter result, use in filter-list component
  employeeTypeApiData: any;

  @Output() filterResult = new EventEmitter<any>();
  @Output() selectedEmployeeType = new EventEmitter<EmployeeTypeViewModel>();

  constructor(
    private employeeTypeService: EmployeeTypeService,
    private filterService: FilterService
  ) {  }

  ngOnInit(): void {
    this.getEmployeeTypes();
  }

  /* ----- HTTP REQUESTS -----*/

  getEmployeeTypesWithFilter(): void {
    this.employeeTypeService.getEmployeeTypesWithFilter()
      .subscribe(data => {
        this.employeeTypeApiData = data;
        this.filterResult.emit(data);
      });
  }

  getEmployeeTypes(): void {
    this.employeeTypeService.getEmployeeTypes()
      .subscribe(data => this.employeeTypeApiData = data);
  }

  /* ----- ACTIONS -----*/

  applyFilter(): void {
    this.getEmployeeTypesWithFilter();
  }

  clearFilters(): void {
    this.employeeTypeService.chosenFilterOptions = this.employeeTypeService.defaultFilterOptions;
    this.resetFilterData();
    this.getEmployeeTypesWithFilter();
  }

  chooseOption(option: string): void {
    const index = this.findChosenOptionIndex(option);
    if (index !== -1) {
      this.employeeTypeService.chosenFilterOptions.splice(index, 1);
    } else {
      this.employeeTypeService.chosenFilterOptions.push(option);
    }
  }

  /* ----- MISC -----*/

  resetFilterData(): void {
    this.employeeTypeService.resetFilterData();
  }

  isOptionChosen(fieldName: string): boolean {
    return this.findChosenOptionIndex(fieldName) !== -1;
  }

  findChosenOptionIndex(fieldName: string): number {
    return this.employeeTypeService.chosenFilterOptions.findIndex(chosenOption => chosenOption === fieldName);
  }

  /* ----- GETTER ----- */

  get isFilterListShown(): boolean {
    return this.filterService.isFilterListShown();
  }

  get fields(): {[key: string]: string} {
    return this.employeeTypeService.fields;
  }

  get filterOptions(): string[] {
    return this.employeeTypeService.filterOptions;
  }

  get fetchItemConditions(): EmployeeType {
    return  this.employeeTypeService.fetchItemConditions;
  }

  get chosenFilterOptions(): string[] {
    return this.employeeTypeService.chosenFilterOptions;
  }

  /* ----- SWITCH FILTER ----- */

  /*  Each dropdown in the filter form of this component will need the following function  */
  showEmployeeTypeFilterComponent(): void {
    this.filterService.addFilter('enterprise.employeeType');
  }


  /* ----- SELECT ITEM ----- */
  employeeTypes: any;

  onSelectEmployeeType(employeeType: EmployeeTypeViewModel): void {
    this.filterService.onSelectItem(employeeType);
  }

}
