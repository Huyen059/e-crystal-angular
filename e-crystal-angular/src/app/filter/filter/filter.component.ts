import {Component, OnInit, Output, EventEmitter, Input} from '@angular/core';
import {FilterService} from '../_services/filter.service';
import {elementDisplayNames} from '../../_properties/elementDisplayNames';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit {
  @Input() showListForFirstFilter: boolean;
  @Output() filterResult = new EventEmitter<any>();
  @Output() selectItem = new EventEmitter<any>();

  constructor(
    private filterService: FilterService
  ) { }

  ngOnInit(): void {
  }

  nameOfElementBeingFiltered(): string {
    return this.filterService.currentElement().elementFullName;
  }

  retrieveFilterResult(filterResult: any): void {
    if (this.filterService.filterData.length === 1) {
      this.filterResult.emit(filterResult);
    }
  }

  get filterBreadcrumbs(): string[] {
    return  this.filterService.getFilterBreadcrumbs();
  }

  getElementDisplayName(elementFullName: string): string {
    return elementDisplayNames[elementFullName];
  }

  displayFilter(elementName: string): void {
    this.filterService.displayFilter(elementName);
  }
}
