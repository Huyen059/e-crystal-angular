import {Injectable} from '@angular/core';
import {FilterInfo} from '../_models/FilterInfo';
import {SelectedField} from '../_models/SelectedField';
import {isNotNullOrUndefined} from 'codelyzer/util/isNotNullOrUndefined';

@Injectable({
  providedIn: 'root'
})
export class FilterService {

  filterData: FilterInfo[] = [];

  constructor() {
  }

  addFilter(elementFullName: string): void {
    if (this.filterData.length === 0 || this.currentElement().elementFullName !== elementFullName) {
      this.filterData.push(new FilterInfo(elementFullName));
    }
  }

  onSelectItem(item: any): void {
    const selectedField = new SelectedField(this.currentElement().elementFullName, item);
    if (this.filterData.length > 1) {
      this.removeLastFilter();
      this.currentElement().selectedField = selectedField;
    } else {
      this.currentElement().selectedItem = selectedField;
    }
  }

  removeLastFilter(): void {
    this.filterData.pop();
  }

  isFilterListShown(): boolean {
    return this.filterData.length > 1;
  }

  currentElement(): FilterInfo {
    return this.filterData[this.filterData.length - 1];
  }

  resetFilterData(): void {
    this.filterData = [];
  }

  hasSelectedField(elementFullName: string): boolean {
    return isNotNullOrUndefined(this.currentElement())
      && this.currentElement().selectedField !== null
      && this.currentElement().selectedField.elementFullName === elementFullName;
  }

  hasSelectedItem(elementFullName: string): boolean {
    return isNotNullOrUndefined(this.currentElement())
      && this.currentElement().elementFullName === elementFullName
      && isNotNullOrUndefined(this.currentElement().selectedItem);
  }

  /* ----- FILTER BREADCRUMB ----- */

  getFilterBreadcrumbs(): string[] {
    const elements = [];
    for (const item of this.filterData) {
      elements.push(item.elementFullName);
    }
    return elements;
  }

  displayFilter(elementFullName: string): void {
    const index = this.findIndexOf(elementFullName);
    this.filterData = this.filterData.slice(0, index + 1);
  }

  /**
   * Return index of element if found
   *
   * Return -1 if not found
   */
  findIndexOf(elementFullName: string): number {
    for (let i = 0; i < this.filterData.length; i++) {
      const item = this.filterData[i];
      if (item.elementFullName === elementFullName) {
        return i;
      }
    }
    return -1;
  }
}
