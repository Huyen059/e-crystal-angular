import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilterComponent } from './filter/filter.component';
import { TimesheetFilterComponent } from './erpTimesheets/timesheet-filter/timesheet-filter.component';
import { TimesheetFilterListComponent } from './erpTimesheets/timesheet-filter-list/timesheet-filter-list.component';
import { EmployeeFilterComponent } from './enterprise/employee-filter/employee-filter.component';
import { EmployeeFilterListComponent } from './enterprise/employee-filter-list/employee-filter-list.component';
import {FormsModule} from '@angular/forms';
import { EmployeeTypeFilterComponent } from './enterprise/employee-type-filter/employee-type-filter.component';
import { EmployeeTypeFilterListComponent } from './enterprise/employee-type-filter-list/employee-type-filter-list.component';
import { EmployeeSearchComponent } from './enterprise/employee-search/employee-search.component';
import {EnterpriseRoutingModule} from '../enterprise/enterprise-routing.module';
import { TimesheetEntryFilterComponent } from './erpTimesheets/timesheet-entry-filter/timesheet-entry-filter.component';
import { TimesheetEntryFilterListComponent } from './erpTimesheets/timesheet-entry-filter-list/timesheet-entry-filter-list.component';



@NgModule({
  declarations: [
    FilterComponent,
    TimesheetFilterComponent,
    TimesheetFilterListComponent,
    EmployeeFilterComponent,
    EmployeeFilterListComponent,
    EmployeeSearchComponent,
    EmployeeTypeFilterComponent,
    EmployeeTypeFilterListComponent,
    TimesheetEntryFilterComponent,
    TimesheetEntryFilterListComponent
  ],
  exports: [
    FilterComponent
  ],
    imports: [
        CommonModule,
        FormsModule,
        EnterpriseRoutingModule,
    ]
})
export class FilterModule { }
