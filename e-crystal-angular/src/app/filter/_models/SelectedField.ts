export class SelectedField {
  constructor(
    public elementFullName: string = null,
    public value: any = null
  ) {  }
}
