import {SelectedField} from './SelectedField';

/**
 * 'elementFullName': name of the current element being filtered
 *
 * 'filterResult': list of current element objects retrieved from backend
 *
 * 'selectedItem': the selected item of current filter
 * (not to be confused with 'selectedField', the selected item for a field of current filter)
 *
 * 'selectedField': if user uses another filter to search for value of a field, the chosen value emit from that filter will be saved here
 */
export class FilterInfo {
  constructor(
    public elementFullName: string = null,
    public filterResult: any = null,
    public selectedItem: SelectedField = null,
    public selectedField: SelectedField = null,
  ) {  }
}
