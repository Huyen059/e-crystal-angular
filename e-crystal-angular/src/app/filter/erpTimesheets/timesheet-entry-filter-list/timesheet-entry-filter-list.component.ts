import { ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import {LinksModel} from '../../../_models/LinksModel';
import {PageModel} from '../../../_models/PageModel';
import {TimesheetEntryOutputModel} from '../../../erp-timesheets/timesheet-entry/_models/TimesheetEntryOutputModel';
import {TimesheetEntryService} from '../../../erp-timesheets/timesheet-entry/_services/timesheet-entry.service';

@Component({
  selector: 'app-timesheet-entry-filter-list',
  templateUrl: './timesheet-entry-filter-list.component.html',
  styleUrls: ['./timesheet-entry-filter-list.component.css']
})
export class TimesheetEntryFilterListComponent implements OnChanges {
  @Input() timesheetEntryApiData: any;
  @Output() selectedTimesheetEntry = new EventEmitter<TimesheetEntryOutputModel>();

  timesheetEntries: TimesheetEntryOutputModel[];
  links: LinksModel;
  page: PageModel;

  constructor(
    private timesheetEntryService: TimesheetEntryService,
    private changeDetectorRef: ChangeDetectorRef
  ) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.timesheetEntryApiData) {
      this.extractData(changes.timesheetEntryApiData.currentValue);
    }
    this.changeDetectorRef.detectChanges();
  }

  /* ----- HTTP REQUESTS -----*/

  getTimesheetEntriesByPageUrl(url: string): void {
    this.timesheetEntryService.getTimesheetEntriesByPageUrl(url)
      .subscribe(
        data => {
          this.extractData(data);
        }
      );
  }

  /* ----- SELECT ITEM ----- */

  selectTimesheetEntry(timesheetEntry: TimesheetEntryOutputModel): void {
    this.selectedTimesheetEntry.emit(timesheetEntry);
  }

  /* ----- PAGINATION -----*/

  displayFirstPageOfList(): void {
    this.getTimesheetEntriesByPageUrl(this.links.first.href);
  }

  displayPreviousPageOfList(): void {
    if (this.links.prev) {
      this.getTimesheetEntriesByPageUrl(this.links.prev.href);
    }
  }

  displayNextPageOfList(): void {
    if (this.links.next) {
      this.getTimesheetEntriesByPageUrl(this.links.next.href);
    }
  }

  displayLastPageOfList(): void {
    this.getTimesheetEntriesByPageUrl(this.links.last.href);
  }

  /* ----- MISC ----- */

  private extractData(data): void {
    this.timesheetEntries = data._embedded.timesheetEntries;
    this.links = data._links;
    this.page = data._page;
  }
}
