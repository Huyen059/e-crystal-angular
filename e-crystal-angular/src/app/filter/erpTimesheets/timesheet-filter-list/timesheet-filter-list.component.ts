import {Component, Input, OnChanges, Output, SimpleChanges, EventEmitter, ChangeDetectorRef} from '@angular/core';
import {TimesheetOutputModel} from '../../../erp-timesheets/timesheet/_models/TimesheetOutputModel';
import {LinksModel} from '../../../_models/LinksModel';
import {PageModel} from '../../../_models/PageModel';
import {TimesheetService} from '../../../erp-timesheets/timesheet/_services/timesheet.service';

@Component({
  selector: 'app-timesheet-filter-list',
  templateUrl: './timesheet-filter-list.component.html',
  styleUrls: ['./timesheet-filter-list.component.css']
})
export class TimesheetFilterListComponent implements OnChanges {
  @Input() timesheetApiData: any;
  @Output() selectedTimesheet = new EventEmitter<TimesheetOutputModel>();

  timesheets: TimesheetOutputModel[];
  links: LinksModel;
  page: PageModel;

  constructor(
    private timesheetService: TimesheetService,
    private changeDetectorRef: ChangeDetectorRef
  ) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.timesheetApiData) {
      this.extractData(changes.timesheetApiData.currentValue);
    }
    this.changeDetectorRef.detectChanges();
  }

  /* ----- HTTP REQUESTS -----*/

  getTimesheetsByPageUrl(url: string): void {
    this.timesheetService.getTimesheetsByPageUrl(url)
      .subscribe(
        data => {
          this.extractData(data);
        }
      );
  }

  /* ----- SELECT ITEM ----- */

  selectTimesheet(timesheet: TimesheetOutputModel): void {
    this.selectedTimesheet.emit(timesheet);
  }

  /* ----- PAGINATION -----*/

  displayFirstPageOfList(): void {
    this.getTimesheetsByPageUrl(this.links.first.href);
  }

  displayPreviousPageOfList(): void {
    if (this.links.prev) {
      this.getTimesheetsByPageUrl(this.links.prev.href);
    }
  }

  displayNextPageOfList(): void {
    if (this.links.next) {
      this.getTimesheetsByPageUrl(this.links.next.href);
    }
  }

  displayLastPageOfList(): void {
    this.getTimesheetsByPageUrl(this.links.last.href);
  }

  /* ----- MISC ----- */

  private extractData(data): void {
    this.timesheets = data._embedded.timesheets;
    this.links = data._links;
    this.page = data._page;
  }
}
