import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TimesheetOutputModel} from '../../../erp-timesheets/timesheet/_models/TimesheetOutputModel';
import {EmployeeOutputModel} from '../../../enterprise/employee/_models/EmployeeOutputModel';
import {TimesheetService} from '../../../erp-timesheets/timesheet/_services/timesheet.service';
import {EmployeeService} from '../../../enterprise/employee/_services/employee.service';
import {Timesheet} from '../../../erp-timesheets/timesheet/_models/Timesheet';
import {FilterService} from '../../_services/filter.service';
import {PaginatedResponse} from '../../../erp-timesheets/timesheet/_models/PaginatedResponse';

@Component({
  selector: 'app-timesheet-filter',
  templateUrl: './timesheet-filter.component.html',
  styleUrls: ['./timesheet-filter.component.css']
})
export class TimesheetFilterComponent implements OnInit {

  @Input() showListForFirstFilter: boolean;
  // filter result, use in filter-list component
  timesheetApiData: PaginatedResponse;

  // dropdown manipulation
  employees: EmployeeOutputModel[] = [];
  isEmployeeOptionsShown = false;

  @Output() filterResult = new EventEmitter<any>();
  @Output() selectedTimesheet = new EventEmitter<TimesheetOutputModel>();

  constructor(
    private timesheetService: TimesheetService,
    private employeeService: EmployeeService,
    private filterService: FilterService
  ) {
  }

  ngOnInit(): void {
    if (this.isFilterListShown || this.showListForFirstFilter) {
      this.getTimesheets();
    }
  }

  /* ----- HTTP REQUESTS -----*/

  getTimesheets(): void {
    this.timesheetService.getTimesheets()
      .subscribe(data => {
        this.timesheetApiData = data;
        this.filterResult.emit(data);
      });
  }

  getEmployees(): void {
    this.employeeService.getEmployees()
      .subscribe(data => this.employees = data._embedded.employees);
  }

  /* ----- ACTIONS -----*/

  applyFilter(): void {
    this.getTimesheets();
    this.timesheetService.updateHasFilter();
  }

  clearFilters(): void {
    this.timesheetService.resetFilter();
    this.getTimesheets();
  }

  chooseFilterOption(option: string): void {
    const index = this.findIndexOfChosenFilterOption(option);
    if (index !== -1) {
      this.timesheetService.chosenFilterOptions.splice(index, 1);
      this.timesheetService.fetchItemConditions[option] = null;
    } else {
      this.timesheetService.chosenFilterOptions.push(option);
    }
  }

  /* ----- DROPDOWN INPUT ----- */

  toggleDropdownOptions(elementName: string): void {
    if (elementName === 'enterprise.employee') {
      if (!this.isEmployeeOptionsShown) {
        this.getEmployees();
        this.isEmployeeOptionsShown = true;
      } else {
        this.isEmployeeOptionsShown = false;
      }
    }
  }

  closeDropdownOptions(elementName: string): void {
    if (elementName === 'enterprise.employee') {
      this.isEmployeeOptionsShown = false;
    }
  }

  chooseADropdownOption(
    elementName: string,
    option:
      // for each linked field
      EmployeeOutputModel
  ): void {
    // for each linked field
    if (elementName === 'enterprise.employee') {
      this.isEmployeeOptionsShown = false;
      this.timesheetService.fetchItemConditions.employee = option as EmployeeOutputModel;
    }
  }

  showFilterComponentForCurrentField(elementName: string): void {
    this.closeDropdownOptions(elementName);
    this.filterService.addFilter(elementName);
  }

  clearInputField(elementName: string): void {
    // for each linked field
    if (elementName === 'enterprise.employee') {
      this.timesheetService.fetchItemConditions.employee = null;
    }
  }

  /* ----- MISC -----*/

  resetFilterData(): void {
    this.timesheetService.resetFilter();
  }

  isFilterOptionChosen(fieldName: string): boolean {
    return this.findIndexOfChosenFilterOption(fieldName) !== -1;
  }

  findIndexOfChosenFilterOption(fieldName: string): number {
    return this.timesheetService.findIndexOfChosenFilterOption(fieldName);
  }

  /* ----- GETTER ----- */

  get isFilterListShown(): boolean {
    return this.filterService.isFilterListShown();
  }

  get fields(): { [key: string]: string } {
    return this.timesheetService.fields;
  }

  get filterOptions(): string[] {
    return this.timesheetService.filterOptions;
  }

  get fetchItemConditions(): Timesheet {
    return this.timesheetService.fetchItemConditions;
  }

  get chosenFilterOptions(): string[] {
    return this.timesheetService.chosenFilterOptions;
  }

  /*  Each dropdown in the filter form of this component will need the following function  */
  get chosenEmployee(): EmployeeOutputModel {
    const elementFullName = 'enterprise.employee';
    if (this.filterService.hasSelectedField(elementFullName)) {
      // set the selected value saved in filter component to fetchItemCondition object
      this.timesheetService.fetchItemConditions.employee = this.filterService.currentElement().selectedField.value;
      // remove the saved value in filter component so that the selected value is saved only in the fetchItemCondition object
      this.filterService.currentElement().selectedField = null;
    }

    return this.timesheetService.fetchItemConditions.employee;
  }

  /* ----- SELECT ITEM ----- */

  onSelectTimesheet(timesheet: TimesheetOutputModel): void {
    this.filterService.onSelectItem(timesheet);
  }
}
