import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {PaginatedResponse} from '../../../erp-timesheets/timesheet-entry/_models/PaginatedResponse';
import {EmployeeOutputModel} from '../../../enterprise/employee/_models/EmployeeOutputModel';
import {TimesheetEntryOutputModel} from '../../../erp-timesheets/timesheet-entry/_models/TimesheetEntryOutputModel';
import {TimesheetEntryService} from '../../../erp-timesheets/timesheet-entry/_services/timesheet-entry.service';
import {EmployeeService} from '../../../enterprise/employee/_services/employee.service';
import {FilterService} from '../../_services/filter.service';
import {TimesheetEntry} from '../../../erp-timesheets/timesheet-entry/_models/TimesheetEntry';
import {TimesheetOutputModel} from '../../../erp-timesheets/timesheet/_models/TimesheetOutputModel';
import {ProjectOutputModel} from '../../../enterprise/project/_models/ProjectOutputModel';
import {SubprojectOutputModel} from '../../../enterprise/subproject/_models/SubprojectOutputModel';
import {WorkingMonthOutputModel} from '../../../enterprise/working-month/_models/WorkingMonthOutputModel';
import {TimesheetService} from '../../../erp-timesheets/timesheet/_services/timesheet.service';
import {ProjectService} from '../../../enterprise/project/_services/project.service';
import {SubprojectService} from '../../../enterprise/subproject/_services/subproject.service';
import {WorkingMonthService} from '../../../enterprise/working-month/_services/working-month.service';

@Component({
  selector: 'app-timesheet-entry-filter',
  templateUrl: './timesheet-entry-filter.component.html',
  styleUrls: ['./timesheet-entry-filter.component.css']
})
export class TimesheetEntryFilterComponent implements OnInit {

  @Input() showListForFirstFilter: boolean;
  // filter result, use in filter-list component
  timesheetEntryApiData: PaginatedResponse;

  // dropdown manipulation
  employees: EmployeeOutputModel[] = [];
  isEmployeeOptionsShown = false;

  timesheets: TimesheetOutputModel[] = [];
  isTimesheetOptionsShown = false;

  projects: ProjectOutputModel[] = [];
  isProjectOptionsShown = false;

  subprojects: SubprojectOutputModel[] = [];
  isSubprojectOptionsShown = false;

  workingMonths: WorkingMonthOutputModel[] = [];
  isWorkingMonthOptionsShown = false;

  @Output() filterResult = new EventEmitter<any>();
  @Output() selectedTimesheetEntry = new EventEmitter<TimesheetEntryOutputModel>();

  constructor(
    private timesheetEntryService: TimesheetEntryService,
    private employeeService: EmployeeService,
    private timesheetService: TimesheetService,
    private projectService: ProjectService,
    private subprojectService: SubprojectService,
    private workingMonthService: WorkingMonthService,
    private filterService: FilterService
  ) {
  }

  ngOnInit(): void {
    if (this.isFilterListShown || this.showListForFirstFilter) {
      this.getTimesheetEntries();
    }
  }

  /* ----- HTTP REQUESTS -----*/

  getTimesheetEntries(): void {
    this.timesheetEntryService.getTimesheetEntries()
      .subscribe(data => {
        this.timesheetEntryApiData = data;
        this.filterResult.emit(data);
      });
  }

  getEmployees(): void {
    this.employeeService.getEmployees()
      .subscribe(data => this.employees = data._embedded.employees);
  }

  getTimesheets(): void {
    this.timesheetService.getTimesheets()
      .subscribe(data => this.timesheets = data._embedded.timesheets);
  }

  getProjects(): void {
    this.projectService.getProjects()
      .subscribe(data => this.projects = data._embedded.projects);
  }

  getSubprojects(): void {
    this.subprojectService.getSubprojects()
      .subscribe(data => this.subprojects = data._embedded.subprojects);
  }

  getWorkingMonths(): void {
    this.workingMonthService.getWorkingMonths()
      .subscribe(data => this.workingMonths = data._embedded.workingMonths);
  }

  /* ----- ACTIONS -----*/

  applyFilter(): void {
    this.getTimesheetEntries();
    this.timesheetEntryService.updateHasFilter();
  }

  clearFilters(): void {
    this.timesheetEntryService.resetFilter();
    this.getTimesheetEntries();
  }

  chooseFilterOption(option: string): void {
    const index = this.findIndexOfChosenFilterOption(option);
    if (index !== -1) {
      this.timesheetEntryService.chosenFilterOptions.splice(index, 1);
      this.timesheetEntryService.fetchItemConditions[option] = null;
    } else {
      this.timesheetEntryService.chosenFilterOptions.push(option);
    }
  }

  /* ----- DROPDOWN INPUT ----- */

  toggleDropdownOptions(elementName: string): void {
    if (elementName === 'enterprise.employee') {
      if (!this.isEmployeeOptionsShown) {
        this.getEmployees();
        this.isEmployeeOptionsShown = true;
      } else {
        this.isEmployeeOptionsShown = false;
      }
    }

    if (elementName === 'erpTimesheets.timesheet') {
      if (!this.isTimesheetOptionsShown) {
        this.getTimesheets();
        this.isTimesheetOptionsShown = true;
      } else {
        this.isTimesheetOptionsShown = false;
      }
    }

    if (elementName === 'enterprise.project') {
      if (!this.isProjectOptionsShown) {
        this.getProjects();
        this.isProjectOptionsShown = true;
      } else {
        this.isProjectOptionsShown = false;
      }
    }

    if (elementName === 'enterprise.subproject') {
      if (!this.isSubprojectOptionsShown) {
        this.getSubprojects();
        this.isSubprojectOptionsShown = true;
      } else {
        this.isSubprojectOptionsShown = false;
      }
    }

    if (elementName === 'enterprise.workingMonth') {
      if (!this.isWorkingMonthOptionsShown) {
        this.getWorkingMonths();
        this.isWorkingMonthOptionsShown = true;
      } else {
        this.isWorkingMonthOptionsShown = false;
      }
    }

  }

  closeDropdownOptions(elementName: string): void {
    if (elementName === 'enterprise.employee') {
      this.isEmployeeOptionsShown = false;
    }

    if (elementName === 'erpTimesheets.timesheet') {
      this.isTimesheetOptionsShown = false;
    }

    if (elementName === 'enterprise.project') {
      this.isProjectOptionsShown = false;
    }

    if (elementName === 'enterprise.subproject') {
      this.isSubprojectOptionsShown = false;
    }

    if (elementName === 'enterprise.workingMonth') {
      this.isWorkingMonthOptionsShown = false;
    }
  }

  chooseADropdownOption(
    elementName: string,
    option:
      // for each linked field
      EmployeeOutputModel |
      TimesheetOutputModel |
      ProjectOutputModel |
      SubprojectOutputModel |
      WorkingMonthOutputModel
  ): void {
    // for each linked field
    if (elementName === 'enterprise.employee') {
      this.isEmployeeOptionsShown = false;
      this.timesheetEntryService.fetchItemConditions.employee = option as EmployeeOutputModel;
    }

    if (elementName === 'erpTimesheets.timesheet') {
      this.isTimesheetOptionsShown = false;
      this.timesheetEntryService.fetchItemConditions.timesheet = option as TimesheetOutputModel;
    }

    if (elementName === 'enterprise.project') {
      this.isProjectOptionsShown = false;
      this.timesheetEntryService.fetchItemConditions.project = option as ProjectOutputModel;
    }

    if (elementName === 'enterprise.subproject') {
      this.isSubprojectOptionsShown = false;
      this.timesheetEntryService.fetchItemConditions.subproject = option as SubprojectOutputModel;
    }

    if (elementName === 'enterprise.workingMonth') {
      this.isWorkingMonthOptionsShown = false;
      this.timesheetEntryService.fetchItemConditions.workingMonth = option as WorkingMonthOutputModel;
    }
  }

  showFilterComponentForCurrentField(elementName: string): void {
    this.closeDropdownOptions(elementName);
    this.filterService.addFilter(elementName);
  }

  clearInputField(elementName: string): void {
    // for each linked field
    if (elementName === 'enterprise.employee') {
      this.timesheetEntryService.fetchItemConditions.employee = null;
    }

    if (elementName === 'erpTimesheets.timesheet') {
      this.timesheetEntryService.fetchItemConditions.timesheet = null;
    }

    if (elementName === 'enterprise.project') {
      this.timesheetEntryService.fetchItemConditions.project = null;
    }

    if (elementName === 'enterprise.subproject') {
      this.timesheetEntryService.fetchItemConditions.subproject = null;
    }

    if (elementName === 'enterprise.workingMonth') {
      this.timesheetEntryService.fetchItemConditions.workingMonth = null;
    }
  }

  /* ----- MISC -----*/

  resetFilterData(): void {
    this.timesheetEntryService.resetFilter();
  }

  isFilterOptionChosen(fieldName: string): boolean {
    return this.findIndexOfChosenFilterOption(fieldName) !== -1;
  }

  findIndexOfChosenFilterOption(fieldName: string): number {
    return this.timesheetEntryService.findIndexOfChosenFilterOption(fieldName);
  }

  /* ----- GETTER ----- */

  get isFilterListShown(): boolean {
    return this.filterService.isFilterListShown();
  }

  get fields(): { [key: string]: string } {
    return this.timesheetEntryService.fields;
  }

  get filterOptions(): string[] {
    return this.timesheetEntryService.filterOptions;
  }

  get fetchItemConditions(): TimesheetEntry {
    return this.timesheetEntryService.fetchItemConditions;
  }

  get chosenFilterOptions(): string[] {
    return this.timesheetEntryService.chosenFilterOptions;
  }

  /*  Each dropdown in the filter form of this component will need the following function  */
  get chosenEmployee(): EmployeeOutputModel {
    const elementFullName = 'enterprise.employee';
    if (this.filterService.hasSelectedField(elementFullName)) {
      // set the selected value saved in filter component to fetchItemCondition object
      this.timesheetEntryService.fetchItemConditions.employee = this.filterService.currentElement().selectedField.value;
      // remove the saved value in filter component so that the selected value is saved only in the fetchItemCondition object
      this.filterService.currentElement().selectedField = null;
    }

    return this.timesheetEntryService.fetchItemConditions.employee;
  }

  get chosenTimesheet(): TimesheetOutputModel {
    const elementFullName = 'erpTimesheets.timesheet';
    if (this.filterService.hasSelectedField(elementFullName)) {
      // set the selected value saved in filter component to fetchItemCondition object
      this.timesheetEntryService.fetchItemConditions.timesheet = this.filterService.currentElement().selectedField.value;
      // remove the saved value in filter component so that the selected value is saved only in the fetchItemCondition object
      this.filterService.currentElement().selectedField = null;
    }

    return this.timesheetEntryService.fetchItemConditions.timesheet;
  }

  get chosenProject(): ProjectOutputModel {
    const elementFullName = 'enterprise.project';
    if (this.filterService.hasSelectedField(elementFullName)) {
      // set the selected value saved in filter component to fetchItemCondition object
      this.timesheetEntryService.fetchItemConditions.project = this.filterService.currentElement().selectedField.value;
      // remove the saved value in filter component so that the selected value is saved only in the fetchItemCondition object
      this.filterService.currentElement().selectedField = null;
    }

    return this.timesheetEntryService.fetchItemConditions.project;
  }

  get chosenSubproject(): SubprojectOutputModel {
    const elementFullName = 'enterprise.subproject';
    if (this.filterService.hasSelectedField(elementFullName)) {
      // set the selected value saved in filter component to fetchItemCondition object
      this.timesheetEntryService.fetchItemConditions.subproject = this.filterService.currentElement().selectedField.value;
      // remove the saved value in filter component so that the selected value is saved only in the fetchItemCondition object
      this.filterService.currentElement().selectedField = null;
    }

    return this.timesheetEntryService.fetchItemConditions.subproject;
  }

  get chosenWorkingMonth(): WorkingMonthOutputModel {
    const elementFullName = 'enterprise.workingMonth';
    if (this.filterService.hasSelectedField(elementFullName)) {
      // set the selected value saved in filter component to fetchItemCondition object
      this.timesheetEntryService.fetchItemConditions.workingMonth = this.filterService.currentElement().selectedField.value;
      // remove the saved value in filter component so that the selected value is saved only in the fetchItemCondition object
      this.filterService.currentElement().selectedField = null;
    }

    return this.timesheetEntryService.fetchItemConditions.workingMonth;
  }

  /* ----- SELECT ITEM ----- */

  onSelectTimesheetEntry(timesheetEntry: TimesheetEntryOutputModel): void {
    this.filterService.onSelectItem(timesheetEntry);
  }
}
