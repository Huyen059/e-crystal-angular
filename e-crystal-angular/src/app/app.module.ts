import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {HeaderComponent} from './header/header.component';
import {NavComponent} from './nav/nav.component';
import {PageHeaderComponent} from './page-header/page-header.component';
import {LoginComponent} from './login/login.component';
import { PageFooterComponent } from './page-footer/page-footer.component';

import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';

import {AlertModule} from './alert/alert.module';

import {ErpTimesheetsModule} from './erp-timesheets/erp-timesheets.module';
import {EnterpriseModule} from './enterprise/enterprise.module';
import {CrmModelModule} from './crm-model/crm-model.module';

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    HeaderComponent,
    NavComponent,
    PageHeaderComponent,
    LoginComponent,
    PageFooterComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ErpTimesheetsModule,
    EnterpriseModule,
    CrmModelModule,
    AlertModule,
    AppRoutingModule
  ],
  providers: [],
  exports: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
