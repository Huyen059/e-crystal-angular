import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CrmModelRoutingModule } from './crm-model-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CrmModelRoutingModule
  ]
})
export class CrmModelModule { }
