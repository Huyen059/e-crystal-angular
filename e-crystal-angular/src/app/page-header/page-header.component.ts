import {AfterViewInit, Component, OnInit} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {User} from '../_models/User';
import {AuthService} from '../_services/auth.service';

@Component({
  selector: 'app-page-header',
  templateUrl: './page-header.component.html',
  styleUrls: ['./page-header.component.css']
})
export class PageHeaderComponent implements OnInit, AfterViewInit {
  pageTitle: string;
  breadcrumbs: { label: string, url: string }[];
  currentUser: User;
  isUserMenuDisplayed = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthService,
  ) {
  }

  ngOnInit(): void {
    this.currentUser = JSON.parse(sessionStorage.getItem('currentUser'));
    this.makeBreadcrumbs();
  }

  ngAfterViewInit(): void {
    this.userAvatarHandler();
  }

  private makeBreadcrumbs() {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.breadcrumbs = [];
        const data: any = this.router.routerState.root.firstChild.data;
        this.pageTitle = data._value.name;
        let currentRoute = this.router.routerState.root;
        let url = '';
        do {
          const childrenRoutes = currentRoute.children;
          currentRoute = null;
          childrenRoutes.forEach(route => {
            if (route.outlet === 'primary') {
              const routeSnapshot = route.snapshot;
              let name = '';

              if (routeSnapshot.data.name) {
                name = routeSnapshot.data.name;
              }
              const path = routeSnapshot.url.map(segment => segment.path).join('/');
              if (path) {
                url += '/' + path;
              }

              this.breadcrumbs.push(
                {
                  label: name,
                  url: url
                }
              );
              currentRoute = route;
            }
          });
        } while (currentRoute)
      }
    });
  }

  private userAvatarHandler() {
    const userAvatar = document.querySelector('.app-header-user-avatar-component') as HTMLElement;
    const userMenu = document.querySelector('.app-header-user-menu') as HTMLElement;
    userAvatar.addEventListener('click', () => {
      this.isUserMenuDisplayed = !this.isUserMenuDisplayed;
      if (this.isUserMenuDisplayed) {
        userMenu.style.display = 'block';
      } else {
        userMenu.style.display = 'none';
      }
    })

    window.onclick = (e) => {
      if (!userMenu.contains(e.target) && !userAvatar.contains(e.target)) {
        this.isUserMenuDisplayed = false;
        userMenu.style.display = 'none';
      }
    }
  }

  logout() {
    this.authService.logout();
    window.location.replace(this.router.routerState.snapshot.url);
  }

  goToLoginPage() {
    this.router.navigate(['/login'], {queryParams: {returnUrl: this.router.routerState.snapshot.url}});
  }
}
