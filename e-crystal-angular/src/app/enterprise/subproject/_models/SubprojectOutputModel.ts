export class SubprojectOutputModel {
  name: string;
  externalId: string;
  project: {
    resourceUri: string,
    name: string,
    externalId: string
  };
}
