export class Subproject {
  constructor(
    public name: string = null,
    public externalId: string = null,
    public project: string = null,
  ) {  }
}
