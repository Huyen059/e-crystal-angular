export class SubprojectReferenceModel {
  resourceUri: string;
  name: string;
  externalId: string;
}
