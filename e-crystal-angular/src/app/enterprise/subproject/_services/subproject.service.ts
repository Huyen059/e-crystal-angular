import { Injectable } from '@angular/core';
import {authorizationData} from '../../../../../credential';
import {HttpClient} from '@angular/common/http';
import {MessageService} from '../../../_services/message.service';
import {Observable, of} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import {SubprojectOutputModel} from '../_models/SubprojectOutputModel';
import {SubprojectInputModel} from '../_models/SubprojectInputModel';
import {Subproject} from '../_models/Subproject';


@Injectable({
  providedIn: 'root'
})
export class SubprojectService {

  private apiBaseUrl = 'http://localhost:9500/e-crystal/api/v1/' + 'enterprise' + '/subprojects';
  private httpReadOptions = {headers: { Authorization : `Basic ${authorizationData}` }};
  private httpWriteOptions = {headers: { Authorization : `Basic ${authorizationData}`, 'Content-Type': 'application/json' }};
  fetchItemConditions = new Subproject();

  constructor(
    private http: HttpClient,
    private messageService: MessageService,
  ) { }

  // ----- READ ----- //

  getSubprojects(): Observable<any> {
    const url = this.apiBaseUrl;
    return this.http.get(url, this.httpReadOptions)
      .pipe(
        tap(_ => this.log('fetched subprojects')),
        catchError(this.handleError('Get subprojects', []))
      );
  }

  getSubprojectByExternalId(selectedId: string): Observable<any> {
    const url = this.apiBaseUrl + `/${selectedId}`;
    return this.http.get<SubprojectOutputModel>(url, this.httpReadOptions)
      .pipe(
        tap(_ => this.log('fetched subproject with external id = ' + selectedId)),
        catchError(this.handleError('Get subproject by external id'))
      );
  }

  search(term: string): Observable<any> {
    this.fetchItemConditions.name = term.trim();
    return this.getSubprojects();
  }

  // ----- CREATE ----- //

  createSubproject(subproject: SubprojectInputModel): Observable<any> {
    const url = this.apiBaseUrl;
    return this.http.post(url, subproject, this.httpWriteOptions)
      .pipe(
        tap((newSubproject: SubprojectOutputModel) => this.log('create new subproject with external id = ' + newSubproject.externalId)),
        catchError(this.handleError('Get subproject by external id'))
      );
  }

  // ----- UPDATE ----- //

  saveSubproject(subproject: SubprojectInputModel): Observable<any> {
    const url = this.apiBaseUrl + `/${subproject.externalId}`;
    return this.http.put(url, subproject, this.httpWriteOptions)
      .pipe(
        tap(_ => this.log('update subproject')),
        catchError(this.handleError('Update subproject'))
      );
  }

  // ----- DELETE ----- //

  delete(externalId: string): Observable<any> {
    const url = this.apiBaseUrl + `/${externalId}`;
    return this.http.delete(url, this.httpWriteOptions)
      .pipe(
        tap(_ => this.log('delete subproject with external id = ' + externalId)),
        catchError(this.handleError('Delete subproject'))
      );
  }

  // ----- ERROR HANDLING ----- //

  private handleError<T>(operation = 'operation', result?: T): (error: any) => Observable<T> {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  // ----- LOGGING ----- //

  private log(message: string): void {
    this.messageService.add(`SubprojectService: ${message}`);
  }
}
