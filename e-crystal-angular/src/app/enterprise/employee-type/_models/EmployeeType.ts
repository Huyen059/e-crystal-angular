export class EmployeeType {
  constructor(
    public externalId: string = null,
    public name: string = null,
    public sortBy: string = null,
    public isAscending: boolean = null,
  ) {  }

}
