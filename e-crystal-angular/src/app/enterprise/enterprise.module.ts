import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { EnterpriseRoutingModule } from './enterprise-routing.module';


@NgModule({
    declarations: [],
    exports: [
    ],
    imports: [
        CommonModule,
        EnterpriseRoutingModule,
        FormsModule
    ]
})
export class EnterpriseModule { }
