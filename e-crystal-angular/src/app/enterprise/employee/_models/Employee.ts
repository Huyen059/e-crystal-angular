import {SourcerViewModel} from '../../sourcer/_models/SourcerViewModel';
import {EmployeeTypeViewModel} from '../../employee-type/_models/EmployeeTypeViewModel';
import {PersonViewModel} from '../../person/_models/PersonViewModel';

export class Employee {
  constructor(
    public active: string = null,
    public contractType: string = null,
    public email: string = null,
    public employeeType: EmployeeTypeViewModel = null,
    public externalId: string = null,
    public mobile: string = null,
    public name: string = null,
    public person: PersonViewModel = null,
    public sourcer: SourcerViewModel = null,
    public sortBy: string = null,
    public isAscending: boolean = null,
  ) {  }
}
