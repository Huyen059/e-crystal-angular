import {TimesheetReferenceModel} from '../../../erp-timesheets/timesheet/_models/TimesheetReferenceModel';

export class EmployeeOutputModel {
  active: string;
  contractType: string;
  email: { value: string };
  employeeType: TimesheetReferenceModel;
  externalId: string;
  mobile: string;
  name: string;
  person: TimesheetReferenceModel;
  sourcer: TimesheetReferenceModel;
}
