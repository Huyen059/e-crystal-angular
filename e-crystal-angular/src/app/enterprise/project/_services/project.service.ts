import { Injectable } from '@angular/core';
import {authorizationData} from '../../../../../credential';
import {HttpClient} from '@angular/common/http';
import {MessageService} from '../../../_services/message.service';
import {Observable, of} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import {ProjectOutputModel} from '../_models/ProjectOutputModel';
import {Project} from '../_models/Project';
import {ProjectInputModel} from '../_models/ProjectInputModel';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  private apiBaseUrl = 'http://localhost:9500/e-crystal/api/v1/' + 'enterprise' + '/projects';
  private httpReadOptions = {headers: { Authorization : `Basic ${authorizationData}` }};
  private httpWriteOptions = {headers: { Authorization : `Basic ${authorizationData}`, 'Content-Type': 'application/json' }};

  displayMap: Map<string, Map<string, boolean>> = new Map<string, Map<string, boolean>>();

  fields = {
    name: 'name',
    subproject: 'subproject',
  };

  sortableFields = [
    this.fields.name,
  ];

  filterOptions = [
    this.fields.name,
    this.fields.subproject,
  ];

  fetchItemConditions: Project = new Project();
  hasFilter: boolean;

  defaultFilterOptions: string[] = [
    this.fields.name,
  ];

  chosenFilterOptions: string[] = [
    this.fields.name,
  ];

  constructor(
    private http: HttpClient,
    private messageService: MessageService,
  ) { }

  // ----- READ ----- //

  getProjects(): Observable<any> {
    const url = this.apiBaseUrl;
    return this.http.get(url, this.httpReadOptions)
      .pipe(
        tap(_ => this.log('fetched projects')),
        catchError(this.handleError('Get projects', []))
      );
  }

  getProjectByExternalId(selectedId: string): Observable<any> {
    const url = this.apiBaseUrl + `/${selectedId}`;
    return this.http.get<ProjectOutputModel>(url, this.httpReadOptions)
      .pipe(
        tap(_ => this.log('fetched project with external id = ' + selectedId)),
        catchError(this.handleError('Get project by external id'))
      );
  }

  getProjectsByPageUrl(url: string): Observable<any> {
    return this.http.get(url, this.httpReadOptions)
      .pipe(
        tap(_ => this.log('fetched projects by page url')),
        catchError(this.handleError('Get projects by page url', []))
      );
  }

  getProjectsWithFilter(): Observable<any> {
    const url = this.apiBaseUrl + '/filter?' + this.createQueryParameters(this.fetchItemConditions);

    return this.http.get(url, this.httpReadOptions)
      .pipe(
        tap(_ => this.log('fetched projects with filter')),
        catchError(this.handleError('Get projects with filter'))
      );
  }

  search(term: string): Observable<any> {
    this.fetchItemConditions.name = term.trim();
    return this.getProjects();
  }

  // ----- CREATE ----- //

  createProject(project: ProjectInputModel): Observable<any> {
    const url = this.apiBaseUrl;
    return this.http.post(url, project, this.httpWriteOptions)
      .pipe(
        tap((newProject: ProjectOutputModel) => this.log('create new project with external id = ' + newProject.externalId)),
        catchError(this.handleError('Get project by external id'))
      );
  }

  // ----- UPDATE ----- //

  saveProject(project: ProjectInputModel): Observable<any> {
    const url = this.apiBaseUrl + `/${project.externalId}`;
    return this.http.put(url, project, this.httpWriteOptions)
      .pipe(
        tap(_ => this.log('update project')),
        catchError(this.handleError('Update project'))
      );
  }

  // ----- DELETE ----- //

  delete(externalId: string): Observable<any> {
    const url = this.apiBaseUrl + `/${externalId}`;
    return this.http.delete(url, this.httpWriteOptions)
      .pipe(
        tap(_ => this.log('delete project with external id = ' + externalId)),
        catchError(this.handleError('Delete project'))
      );
  }

  // ----- ERROR HANDLING ----- //

  private handleError<T>(operation = 'operation', result?: T): (error: any) => Observable<T> {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  // ----- LOGGING ----- //

  private log(message: string): void {
    this.messageService.add(`ProjectService: ${message}`);
  }

  // ----- MISC ----- //

  createQueryParameters(fetchItemConditions: Project): string {
    const parameters: string[] = [];

    if (fetchItemConditions.name) {
      parameters.push('name=' + fetchItemConditions.name);
    }

    if (fetchItemConditions.sortBy) {
      parameters.push('sortBy=' + fetchItemConditions.sortBy);
    }

    if (fetchItemConditions.isAscending !== null) {
      if (fetchItemConditions.isAscending) {
        parameters.push('isAscending=true');
      } else {
        parameters.push('isAscending=false');
      }
    }

    return parameters.join('&');
  }

  resetDefaultFilterOptions(): void {
    this.chosenFilterOptions = this.defaultFilterOptions;
  }

  resetFilterData(): void {
    this.filterOptions.forEach(option => {
      this.fetchItemConditions[option] = null;
    });
  }

  setHasFilter(): void {
    this.hasFilter = false;
    for (const option of this.filterOptions) {
      if (this.fetchItemConditions[option] !== null) {
        this.hasFilter = true;
      }
    }
  }

  /* ----- DISPLAY SUB ITEMS ----- */

  addItemsToDisplayMap(projects: ProjectOutputModel[]): void {
    const value = new Map([
      [this.fields.subproject, false],
    ]);
    projects.forEach(project => {
      this.displayMap.set(project.externalId, value);
    });
  }

  isAnySubitemsDisplayed(): boolean {
    for (const [, itemDisplayMap] of this.displayMap) {
      for (const [, isDisplay] of itemDisplayMap) {
        if (isDisplay) {
          return true;
        }
      }
    }
    return false;
  }

  isSubitemsDisplay(itemExternalId: string, fieldName: string): boolean {
    return this.displayMap.get(itemExternalId).get(fieldName) === true;
  }

  hideAllSubitemsDisplay(): void {
    const value = new Map([
      [this.fields.name, false],
    ]);
    this.displayMap.forEach((oldValue, externalId) => {
      this.displayMap.set(externalId, value);
    });
  }

  toggleDisplaySubitem(itemExternalId: string, fieldName: string): void {
    if (this.displayMap.get(itemExternalId).get(fieldName)) {
      this.hideItem(itemExternalId, fieldName);
    } else {
      this.displayItem(itemExternalId, fieldName);
    }
  }

  displayItem(itemExternalId: string, fieldName: string): void {
    const value = new Map([
      [this.fields.name, fieldName === this.fields.name],
    ]);
    this.displayMap.set(itemExternalId, value);
  }

  hideItem(itemExternalId: string, fieldName: string): void {
    this.displayMap.get(itemExternalId).set(fieldName, false);
  }

}
