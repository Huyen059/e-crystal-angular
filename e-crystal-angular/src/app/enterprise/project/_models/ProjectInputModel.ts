export class ProjectInputModel {
  constructor(
    public name: string = null,
    public clientReference: string = null,
    public developmentBudget: number = null,
    public maintenanceBudget: number = null,
    public invoicingMode: string = null,
    public internalCode: string = null,
    public externalId: string = null,
    public client: string = null,
    public order: string = null,
    public clientResponsible: string = null,
    public projectManager: string = null,
    public technicalLead: string = null,
    public domainAnalyst: string = null,
  ) {  }
}
