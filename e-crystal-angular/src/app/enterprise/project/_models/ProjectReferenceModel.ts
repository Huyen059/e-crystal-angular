export class ProjectReferenceModel {
  resourceUri: string;
  name: string;
  externalId: string;
}
