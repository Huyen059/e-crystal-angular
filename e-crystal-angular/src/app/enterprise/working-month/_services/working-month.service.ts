import { Injectable } from '@angular/core';
import {authorizationData} from '../../../../../credential';
import {HttpClient} from '@angular/common/http';
import {AlertService} from '../../../alert/_services/alert.service';
import {Observable, of} from 'rxjs';
import {PaginatedResponse} from '../_models/PaginatedResponse';
import {catchError, tap} from 'rxjs/operators';
import {enterpriseProperties} from '../../../_properties/enterpriseProperties';
import {WorkingMonth} from '../_models/WorkingMonth';
import {WorkingMonthOutputModel} from '../_models/WorkingMonthOutputModel';
import {WorkingMonthPostInputModel} from '../_models/WorkingMonthPostInputModel';
import {WorkingMonthPutInputModel} from '../_models/WorkingMonthPutInputModel';

@Injectable({
  providedIn: 'root'
})
export class WorkingMonthService {

  private apiBaseUrl = enterpriseProperties.baseUrl + '/workingmonths';
  private httpReadOptions = {headers: { Authorization : `Basic ${authorizationData}` }};
  private httpWriteOptions = {headers: { Authorization : `Basic ${authorizationData}`, 'Content-Type': 'application/json' }};

  fields = {
    name: 'name',
    month: 'month',
    year: 'year',
  };

  filterOptions = [
    this.fields.month,
    this.fields.year,
  ];

  defaultFilterOptions: string[] = [
    this.fields.month,
    this.fields.year,
  ];

  chosenFilterOptions: string[] = [
    this.fields.month,
    this.fields.year,
  ];

  fetchItemConditions: WorkingMonth = new WorkingMonth();
  private _hasFilter: boolean;
  pageUrl: string;

  constructor(
    private http: HttpClient,
    private alertService: AlertService
  ) { }

  /* ----- READ ----- */

  getWorkingMonths(): Observable<PaginatedResponse> {
    let url = this.apiBaseUrl;
    this.updateHasFilter();
    if (this.hasFilter || this.fetchItemConditions.sortBy) {
      url += '?' + WorkingMonthService.createQueryParameters(this.fetchItemConditions);
    }
    return this.http.get<PaginatedResponse>(url, this.httpReadOptions)
      .pipe(
        catchError(this.handleError('Get working months', new PaginatedResponse()))
      );
  }

  getWorkingMonthByExternalId(selectedId: string): Observable<WorkingMonthOutputModel> {
    const url = this.apiBaseUrl + `/${selectedId}`;
    return this.http.get<WorkingMonthOutputModel>(url, this.httpReadOptions)
      .pipe(
        catchError(this.handleError('Get working month by external id', new WorkingMonthOutputModel()))
      );
  }

  getWorkingMonthsByPageUrl(url: string): Observable<PaginatedResponse> {
    this.pageUrl = url;
    return this.http.get<PaginatedResponse>(url, this.httpReadOptions)
      .pipe(
        catchError(this.handleError('Get working months by page url', new PaginatedResponse()))
      );
  }

  search(term: string): Observable<any> {
    this.fetchItemConditions.name = term.trim();
    return this.getWorkingMonths();
  }

  /* ----- CREATE ----- */

  createWorkingMonth(workingMonth: WorkingMonthPostInputModel): Observable<any> {
    const url = this.apiBaseUrl;
    return this.http.post(url, workingMonth, this.httpWriteOptions)
      .pipe(
        tap(_ => this.logSuccess('WorkingMonth created')),
        catchError(this.handleError('Get working month by external id'))
      );
  }

  /* ----- UPDATE ----- */

  saveWorkingMonth(workingMonth: WorkingMonthPutInputModel): Observable<any> {
    const url = this.apiBaseUrl + `/${workingMonth.externalId}`;
    return this.http.put(url, workingMonth, this.httpWriteOptions)
      .pipe(
        tap(_ => this.logSuccess('WorkingMonth updated')),
        catchError(this.handleError('Update working month'))
      );
  }

  /* ----- DELETE ----- */

  delete(externalId: string): Observable<any> {
    const url = this.apiBaseUrl + `/${externalId}`;
    return this.http.delete(url, this.httpWriteOptions)
      .pipe(
        tap(_ => this.logSuccess('WorkingMonth deleted')),
        catchError(this.handleError('Delete working month'))
      );
  }

  /* ----- ERROR HANDLING ----- */

  private handleError<T>(operation = 'Operation', result?: T): (error: any) => Observable<T> {
    return (error: any): Observable<T> => {
      this.logError(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

  /* ----- LOGGING ----- */

  private logSuccess(message: string): void {
    this.alertService.success(`WorkingMonthService: ${message}`, { autoClose: true });
  }

  private logError(message: string): void {
    this.alertService.error(`WorkingMonthService: ${message}`, { autoClose: true });
  }

  /* ----- FILTER ----- */

  private static createQueryParameters(fetchItemConditions: WorkingMonth): string {
    const parameters: string[] = [];

    if (fetchItemConditions.month) {
      parameters.push('month=' + fetchItemConditions.month);
    }

    if (fetchItemConditions.year) {
      parameters.push('year=' + fetchItemConditions.year);
    }

    if (fetchItemConditions.sortBy) {
      parameters.push('sortBy=' + fetchItemConditions.sortBy);
    }

    if (fetchItemConditions.isAscending !== null) {
      if (fetchItemConditions.isAscending) {
        parameters.push('isAscending=true');
      } else {
        parameters.push('isAscending=false');
      }
    }

    return parameters.join('&');
  }

  findIndexOfChosenFilterOption(fieldName: string): number {
    return this.chosenFilterOptions.findIndex(chosenOption => chosenOption === fieldName);
  }

  resetDefaultFilterOptions(): void {
    this.chosenFilterOptions = this.defaultFilterOptions;
  }

  resetFilter(): void {
    this.filterOptions.forEach(option => {
      this.fetchItemConditions[option] = null;
    });
    this.resetDefaultFilterOptions();
    this._hasFilter = false;
  }

  resetFetchItemConditions(): void {
    this.fetchItemConditions = new WorkingMonth();
  }

  updateHasFilter(): void {
    this._hasFilter = false;
    for (const option of this.filterOptions) {
      if (this.fetchItemConditions[option] !== null) {
        this._hasFilter = true;
        break;
      }
    }
  }

  get hasFilter(): boolean {
    return this._hasFilter;
  }
}
