import {WorkingYearReferenceModel} from '../../working-year/_models/WorkingYearReferenceModel';

export class WorkingMonthOutputModel {
  constructor(
    public name: string = null,
    public externalId: string = null,
    public month: string = null,
    public year: WorkingYearReferenceModel = null
  ) {
  }
}

