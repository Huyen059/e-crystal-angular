import {WorkingYearOutputModel} from '../../working-year/_models/WorkingYearOutputModel';

export class WorkingMonth {
  constructor(
    public name: string = null,
    public externalId: string = null,
    public month: string = null,
    public year: WorkingYearOutputModel = null,
    public sortBy: string = null,
    public isAscending: boolean = null,
  ) {
  }
}

