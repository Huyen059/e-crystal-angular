export class WorkingMonthPutInputModel {
  constructor(
    public name: string = null,
    public externalId: string = null,
    public month: string = null,
    public year: string = null
  ) {
  }
}

