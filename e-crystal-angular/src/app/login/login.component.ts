import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../_services/auth.service';
import {AlertService} from '../alert/_services/alert.service';
import {LoginPostInputModel} from '../_models/LoginPostInputModel';
import {User} from '../_models/User';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  submitted = false;
  returnUrl: string;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private alertService: AlertService,
  ) {
    if (this.authService.currentUserValue) {
      this.router.navigate(['/']);
    }
  }

  ngOnInit(): void {

    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    })

    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  get f() {
    return this.loginForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    this.alertService.clear();

    if (this.loginForm.invalid) {
      return;
    }

    let userData = new LoginPostInputModel();
    userData.username = this.f.username.value;
    userData.password = this.f.password.value;

    this.authService.requestLogin(userData)
      .subscribe(response => {
        if (response.authenticated) {
          const user = new User();
          user.username = userData.username;
          user.password = btoa(userData.password);
          user.authorizationData = btoa(userData.username + ":" + userData.password);
          sessionStorage.setItem('currentUser', JSON.stringify(user));
          this.authService.currentUserSubject.next(user);
          window.location.replace(this.returnUrl);
        } else {
          this.alertService.error('Username or password is incorrect');
        }
      });
  }
}
