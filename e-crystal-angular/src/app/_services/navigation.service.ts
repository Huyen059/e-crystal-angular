import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NavigationService {

  parentElementFullName: string;
  parentExternalId: string;

  constructor() { }

}
