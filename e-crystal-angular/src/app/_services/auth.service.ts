import {Injectable} from '@angular/core';
import {authorizationData} from '../../../credential';
import {BehaviorSubject, Observable} from 'rxjs';
import {User} from '../_models/User';
import {HttpClient} from '@angular/common/http';
import {erpTimesheetsProperties} from '../_properties/erpTimesheetsProperties';
import {LoginPostInputModel} from '../_models/LoginPostInputModel';
import {LoginOutputModel} from '../_models/LoginOutputModel';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private apiBaseUrl = erpTimesheetsProperties.baseUrl + '/login';
  private httpWriteOptions = {headers: { Authorization : `Basic ${authorizationData}`, 'Content-Type': 'application/json' }};

  currentUserSubject: BehaviorSubject<User>;
  currentUser: Observable<User>;

  constructor(
    private http: HttpClient
  ) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(sessionStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  requestLogin(userData: LoginPostInputModel) {
    const url = this.apiBaseUrl;

    return this.http.post<LoginOutputModel>(url, userData, this.httpWriteOptions);
  }

  logout(): void {
    sessionStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
  }
}
