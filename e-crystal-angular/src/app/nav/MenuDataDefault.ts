export const menuDataDefault = {
  dropdowns: [
    // Dropdown and its items
    {
      title: "ErpTimesheets",
      items: [
        {name: "Timesheet", link: "/timesheets"},
        {name: "TimesheetEntry", link: "/timesheetentries"},
        {name: "TimesheetRemark", link: "/timesheetremarks"},
      ],
    },

    // Dropdown and its items
    {
      title: "Enterprise",
      items: [
        {name: "Employee", link: "/employees"},
      ],
    },
  ],
}
