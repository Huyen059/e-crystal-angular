export const menuData = {
  dropdowns: [
    // Dropdown and its items
    {
      title: "Timesheets",
      items: [
        {name: "Timesheet", link: "/timesheets"},
        {name: "Timesheet Entry", link: "/timesheetentries"},
        {name: "Timesheet Remark", link: "/timesheetremarks"},
      ],
    },

    // Dropdown and its items
    {
      title: "Enterprise",
      items: [
        {name: "Employee", link: "/employees"},
      ],
    },
  ],
}
